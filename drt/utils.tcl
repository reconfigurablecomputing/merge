########################################################################################
#
# Arguments:
#
# Results:
#
########################################################################################
proc string_occurrences {needleString haystackString} {
    set j [string first $needleString $haystackString 0]
    if {$j == -1} {return 0}

    set i 0
    while {$j != -1 } {
        set j [string first $needleString $haystackString [incr j]]
        incr i
    }
    return $i
}

########################################################################################
# save_var_to_tcl_file
# Arguments:
#
# Results:
#
########################################################################################
proc save_var_to_tcl_file {varname {dir "saved"}} {
  upvar 1 $varname varvalue
  #set dir "saved"
  file mkdir $dir
  set fp [open "$dir/${varname}_saved.tclvar" w+]
  puts $fp "# Note: This file contains a saved tcl variable and can be restored using the source command."
  puts $fp "set ${varname} {${varvalue}}"
  close $fp
}

########################################################################################
# load_var_from_tcl_file
# Arguments:
#
# Results:
# Assigns the value to the varname.
# Returns 1 when the saved file exist and the variable is set from that file.
########################################################################################
proc load_var_from_tcl_file {varname {dir "saved"}} {
  upvar 1 $varname varvalue
  #set dir "saved"
  if {[file exists "$dir/${varname}_saved.tclvar"]} {
    source "$dir/${varname}_saved.tclvar"
    if {[info exist varname]} { set varvalue [set $varname]; return 1; }
  }
  return 0
}
