########################################################################################
# list delete
# Deletes all matching items from a list
#
# Arguments:
#
# Result:
########################################################################################
proc ldelete {the_list itemname} {
  set new_list {}
  foreach item $the_list {
    if {!($item == $itemname)} {
      lappend new_list $item
    }
  }
  return $new_list
}

########################################################################################
#
# Arguments:
#
# Results:
#
########################################################################################
proc list_insert_sublist_at_index {list1 sublist index} {
  set head [lrange $list1 0 $index-1]
  set tail [lrange $list1 $index end]
  return [concat [concat $head $sublist] $tail]
}

########################################################################################
#
# Arguments:
#
# Results:
#
########################################################################################
proc list_diff {a b} {
  set diff [list]
  foreach i $a {
    if { [lsearch -exact $b $i]==-1} {
      lappend diff $i
    }
  }
  return $diff
}

########################################################################################
#
# Arguments:
#
# Results:
#
########################################################################################
proc list_equal {a b} {
  error "Use ::struct::set equal"
  package require struct::set
  return [::struct::set equal $a $b]
}


########################################################################################
#
# https://wiki.tcl-lang.org/page/lmap
# Arguments:
#
# Example:
# lmap i {1 2 3 4 5} {expr {$i * $i}}
#
# Results:
#
########################################################################################
proc lmap {_var list body} {
    upvar 1 $_var var
    set res {}
    foreach var $list {lappend res [uplevel 1 $body]}
    set res
}

########################################################################################
#
# https://wiki.tcl-lang.org/page/Counting+Elements+in+a+List
# Arguments:
#
# Example:
#  lcount {1 1 1 2 2 2 2 2 2 3 3}
#  % 1 3 2 6 3 2
# Results:
# A dict
########################################################################################
proc lcount list {
  set count {}
  foreach element $list {dict incr count $element}
  set count
}


########################################################################################
# lfind_first_element_near_element
#
# Arguments:
#
# Example:
#
# Results:
#
########################################################################################
proc lfind_first_element_near_element {the_list element element_find direction} {
  set idx [lsearch $the_list $element]
  if {!($direction != -1 || $direction != 1)} { error "direction should be -1 or 1."}

  for {set i $idx} {$i > -1 && $i < [llength $the_list]} {incr i $direction} {
    if {[lindex $the_list $i] == $element_find} {
      return $i
    }
  }
  return -1
}

########################################################################################
# lconsecutive
# checks if the numbers in the list are consecutive with 1
# Arguments:
#
# Example:
#
# Results:
#
########################################################################################
proc lconsecutive {llist} {
  for {set i 1} {$i < [llength $llist]} {incr i 1} {
    set j [lindex $llist $i]
    set k [lindex $llist $i-1]
    if {[expr {abs($k -$j)}] !=1} {return 0}
  }

  return 1
}