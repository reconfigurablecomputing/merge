##########################################################################################
# Merge of a module into the partition of the static system
#
# Run this in Vivado with:
#   source Run_Merge.tcl
# or
#   paste into TCL Console of Vivado
#
##########################################################################################
package require Tcl 8.0
package require struct::set
package require struct::list


##########################################################################################
# Script parameters:
##########################################################################################
set path_to_script            [file dirname [file normalize [info script]]]
set path_to_project           $path_to_script
set log_dir                   "$path_to_project/log"
set path_input                "${path_to_project}/Input"
set path_output               "${path_to_project}/Output"
set modules_input             "${path_input}/Modules"
set modules_output            "${path_output}/Modules"
set static_system_input       "${path_input}/StaticSystem"
set static_system_output      "${path_output}/StaticSystem"
set script_mapping            "mapping.tclconfig"
set inst_partialarea          "inst_PartialArea"
set inst_module               "inst_Module"
set inst_connectionprimitive  "inst_ConnectionPrimitive"
##########################################################################################


##########################################################################################
# Some helper procs
##########################################################################################
# Define a restart command for easy debugging (command 'restart' is already used for the simulator, hence rs)
proc rs {{arg {}}} {
  if {[current_project -quiet] != ""} { close_design }

  set path_to_script            [file dirname [file normalize [info script]]]
  set path_to_project           $path_to_script
  set log_dir                   "$path_to_project/log"

  # Clean up the workspace
  if {$arg == "c"} {
    cd $path_to_script
    lappend files {*}[glob -nocomplain *_saved.tclvar]
    lappend files {*}[glob -nocomplain saved/*_saved.tclvar]
    lappend files {*}[glob -nocomplain hs_err_*.log]
    lappend files $log_dir/logger.log
    if {[llength $files] > 0} {file delete {*}$files}
  }
  source $path_to_script/module_cut_out.tcl
}

# Save and open the saved checkpoint
proc save_checkpoint {dcp} {
  write_checkpoint -force $dcp
  close_design
  open_checkpoint $dcp
}
##########################################################################################


##########################################################################################
# internal variables
set static_system_input_dcp [lindex [glob "${static_system_input}/*.dcp"] 0]
set static_system_name  [string map {".dcp" ""} [file tail $static_system_input_dcp]]
set modules_dcp [glob "${modules_input}/*.dcp"]
##########################################################################################


##########################################################################################
# Start
##########################################################################################
cd $path_to_project
exec mkdir -p $log_dir
source list_extensions.tcl
source dict_extensions.tcl
source utils_debug.tcl
source utils.tcl
source utils_net_route.tcl
source partition_nets.tcl
file delete $log_dir/logger.log
logger.info "============================================================================"
logger.info "Start of script [file tail [info script]]."
if {[version -short] != "2018.3"} { logger.warning "Untested in this version of Vivado: [version -short]"}
set_param messaging.defaultLimit 2000
set_param tcl.collectionResultDisplayLimit 0
#partition_nets::create 1


##########################################################################################
# Read the interface mapping from file
##########################################################################################
set script "$path_input/$script_mapping"
unset -nocomplain mapping_config
unset -nocomplain modules_config

if {[file exist $script]==0} { logging.error "Error: Mapping script not found: ${script}" }
source $script
if {![info exists mapping_config]}   { logging.error "Error: Variable 'mapping_config' not set." }
if {![info exists modules_config]}   { logging.error "Error: Variable 'modules_config' not set." }
if {[dict size $mapping_config] < 1} { logging.error "Error: Variable 'mapping_config' is empty" }
if {[llength $modules_config] < 1}   { logging.error "Error: Variable 'modules_config' is empty" }
set interface_map [dict merge [dict create] $mapping_config]
set modules $modules_config
##########################################################################################




foreach module $modules {
  set verbose 1
  set module_dcp "${modules_input}/${module}.dcp"
  if {![file exist $module_dcp]} { logger.warning "Warning: Input DCP file does not exist: $module_dcp."; continue; }

  # Open the dcp file
  open_checkpoint $module_dcp
  logger.info "Module $module"

  # Get all the connection nets from the module
  set nets [get_nets -filter "NAME=~*inst_ConnectionPrimitive* && MARK_DEBUG==1 && DONT_TOUCH==1" -hierarchical]
  if {[llength $nets] <= 0} {logger.error "No interface nets in module found."}
  set module_nets [get_nets [get_nets $nets -segments] -filter "PARENT_CELL==$inst_module"]
  if {[llength $module_nets] <= 0} {logger.error "No module nets found."}

  # Save interface nets
  set module_interface "${module}_interface"
  set $module_interface [dict create]
  foreach module_net $module_nets {
    set connection_nets [get_nets [get_nets $module_net -segments] -filter "NAME=~$inst_connectionprimitive* && MARK_DEBUG==1"]
    dict set $module_interface $module_net $connection_nets
  }
  slvar::save $module_interface

  # Get the absolute route
  set module_route "${module}_route"
  set $module_route [get_abs_route $nets]
  #slvar::save $module_route

  #set module_pblock [get_pblocks -filter "NAME=~*$inst_module"]
  #set grid_ranges [get_property GRID_RANGES [get_pblocks $module_pblock]]
  # logger.debug "$module_pblock"
  # logger.debug "$grid_ranges"
  #set tiles [get_tiles -of_objects [get_sites -of_objects [get_pblocks $module_pblock]]]



  # Get the region of the module on the fabric
  #SB UpperLeftTile=INT_L_X34Y49 LowerRightTile=INT_R_X37Y0;
  #SR UpperLeftTile=INT_L_X38Y49 LowerRightTile=INT_R_X41Y0;
  #MC UpperLeftTile=INT_L_X38Y99 LowerRightTile=INT_R_X41Y50;
  #set modules_AES_config_1 [list SUBBYTES_MsLBsM_0s1_W_NE SHIFT_ROWS_LsMLsM_1s1 MIX_COLUMNS_LsMLsM]
  set xmin -1
  set xmax -1
  set ymin -1
  set ymax -1

  if {$module == "SUBBYTES_MsLBsM_0s1_W_NE"} {
    #SB UpperLeftTile=INT_L_X34Y49 LowerRightTile=INT_R_X37Y0;
    set xmin 34
    set xmax 37
    set ymin 0
    set ymax 49
  }
  if {$module == "SHIFT_ROWS_LsMLsM_1s1"} {
    #SR UpperLeftTile=INT_L_X38Y49 LowerRightTile=INT_R_X41Y0;
    set xmin 38
    set xmax 41
    set ymin 0
    set ymax 49
  }
  if {$module == "MIX_COLUMNS_LsMLsM"} {
    #MC UpperLeftTile=INT_L_X38Y99 LowerRightTile=INT_R_X41Y50;
    set xmin 38
    set xmax 41
    set ymin 50
    set ymax 99
  }


  set _module_route [set $module_route]
  set j 1
  dict for {net route} $_module_route {
    if {$verbose} {logger.debug "($j/[dict size $_module_route]) net: $net"}
    set new_route [list]
    set i 0
    set include 0
    set first_node_was_inside_pblock 0
    set use_original_route_string 0

    foreach node $route {
      # Include the first node, or when we encounter a bracket
      if {($i == 0 || $node == "\{" || $node == "\}")} {set include 1}

      if {[get_xy $node xy]} {
        set x [lindex $xy 0]
        set y [lindex $xy 1]

        if {!($x >= $xmin && $x <= $xmax && $y >= $ymin && $y <= $ymax)} {
          #if {$verbose} {logger.debug "Node '$node' is outside pblock."}
          set include 0
          if {$first_node_was_inside_pblock == 0} {
            logger.info "Using the original route string for net '$net'"
            set use_original_route_string 1
            break;
          }
        } else {
          # First node was inside the pblock, this net is probably an output net.
          set first_node_was_inside_pblock 1
        }
      }

      #if {$verbose} {logger.debug "Node: $node, include $include"}

      if {$include} {lappend new_route $node}
      incr i 1
    }

    if {$use_original_route_string == 0} {
      # Save this route and update the design with the new route
      dict set $module_route $net $new_route
      set_property ROUTE {} [get_nets $net]
      set routestring [get_route_string_from_route_list $new_route]
      set_property ROUTE $routestring [get_nets $net]
    }
    incr j 1
  }

  # Save routing to file
  slvar::save $module_route

  # Note that removing cells, also clears the route property
  set cells [get_cells -filter "NAME=~inst_ConnectionPrimitive*"]
  remove_cell [get_cells $cells]

  set pblocks [get_pblocks -filter "NAME=~*inst_ConnectionPrimitive*"]
  delete_pblocks [get_pblocks $pblocks]

  # Remove some residual nets
  if {[llength [get_nets -quiet i_s2p[*]]] > 0} {
    remove_net i_s2p
  }

  if {[llength [get_nets -quiet i_p2s[*]]] > 0} {
    remove_net i_p2s
  }

  if {[llength [get_nets -quiet blocker_net_BlockSelection]] > 0} {
    remove_net blocker_net_BlockSelection
    remove_cell [list {gnd_for_BlockSelection}]
  }

  # Attempt to remove the GND nets. Since they can have an overlap with the static design.
  #if {$modules == "SUBBYTES_MsLBsM_0s1_W_NE SHIFT_ROWS_LsMLsM_1s1 MIX_COLUMNS_LsMLsM"} {
    # Can we achieve this by just unrouting these nets, or do they reappear?
    # It fails, it does not allow undriven bits:
    # ERROR: [DRC NDRV-1] Driverless Nets: Bus Net inst_Module/SB_IN[127:0] has undriven bits 0:1
    # route_design -unroute -physical_nets
  #}

  set dcp "${modules_output}/${module}_cutout"
  write_checkpoint -force $dcp
  close_project
}

