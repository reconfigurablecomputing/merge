##########################################################################################
#
#
# Run this in Vivado with:
#   source SB_NorthBorder_Nodes.tcl
# or
#   paste into TCL Console of Vivado
#
##########################################################################################
set path_to_script            [file dirname [file normalize [info script]]]
set path_to_project           $path_to_script
set log_dir                   "$path_to_project/log"
set path_input                "${path_to_project}/Input"

cd $path_to_script

source list_extensions.tcl
source dict_extensions.tcl
source logger.tcl
source utils.tcl
source utils_net_route.tcl
source slvar.tcl


proc get_SB_North_nodes {} {
  set topleft_x 31
  set topleft_y 133
  set bottomright_x 40
  set bottomright_y 50
  set node ""
  set nets_route [get_abs_route [get_nets inst_ConnectionPrimitiveNorthOutput/x0y0_p2s_n[*]]]
  set SB_North_nets_node [dict create]

  dict for {net route} $nets_route {
    set node ""
    set i [get_first_node_in_region $route $topleft_x $topleft_y $bottomright_x $bottomright_y node]
    #set value [dict create $node $i]
    set value $node
    dict set SB_North_nets_node $net $value
  }

  # set SB_North_nodes [list]
  # dict for {net node_i} $nets_node {
  #   set node [dict keys $node_i]
  #   lappend SB_North_nodes $node
  # }

  slvar::save SB_North_nets_node
}


open_checkpoint "/home/jeroen/git/Merge/drt/examples/casestudy-aes/config1/input/modules/module_SubBytes_config1.dcp"
get_SB_North_nodes
close_project