##########################################################################################
#
#
# Run this in Vivado with:
#   source SB_NorthBorder_Remove.tcl
# or
#   paste into TCL Console of Vivado
#
##########################################################################################
set path_to_script            [file dirname [file normalize [info script]]]
set path_to_project           $path_to_script
set log_dir                   "$path_to_project/log"
set path_input                "${path_to_project}/Input"

cd $path_to_script

source list_extensions.tcl
source dict_extensions.tcl
source logger.tcl
source utils.tcl
source utils_net_route.tcl

########################################################################################
# remove branch from a list of nodes
#
# Arguments:
#
# Results:
#  returns a list of nodes with the branch removed
########################################################################################
proc remove_branch2 {lnodes node} {
  set nodes $lnodes

  # Find the first closing brace before this node
  logger.debug "lnodes: $lnodes"
  set cb_i [lfind_first_element_near_element $lnodes $node \} 1]
  set node_idx [lindex [lsearch $lnodes $node] 0 ]
  if {$cb_i == -1} {error "No close brace found."}
  if {$node_idx == -1} {error "Node not found."}

  set start_i -1
  set end_i $node_idx

  logger.debug "cb_i: $cb_i"
  logger.debug "node_idx: $node_idx"

  # Find the lowest index before we hit a brace
  for {set i $node_idx} {$i > -1} { incr i -1} {
    set next_node [lindex $lnodes $i-1]

    if {($next_node == "\}" || $next_node == "\{")} {
      set start_i $i

      # Found an open brace: node is part of a single branch
      if {$next_node == "\{"} {
        set start_i [expr {$i-1}]
        set end_i $cb_i
      } else {
        set end_i [expr {$cb_i -1}]
      }
      break
    }
  }

  logger.debug "start_i: $start_i"
  logger.debug "start_i node: [lindex $lnodes $start_i ]"
  logger.debug "end_i node: [lindex $lnodes $end_i ]"

  # Remove the branch
  set nodes [lreplace $nodes $start_i $end_i]

  # Remove any double closing braces
  remove_double_close_braces nodes

  # In case everthing got deleted, return empty node list instead
  if {$nodes == ""} {set nodes [list \{ \}] }

  return $nodes
}



proc remove_SB_North_branch {} {
  set topleft_x 31
  set topleft_y 133
  set bottomright_x 40
  set bottomright_y 50
  set node ""
  set nets_route [get_abs_route [get_nets inst_ConnectionPrimitiveNorthOutput/x0y0_p2s_n[*]]]
  set SB_North_nets_node [dict create]
  set _nets_route $nets_route

  set i 1
  set size [dict size $_nets_route]
  dict for {net route} $_nets_route {
    set node ""
    set idx [get_first_node_in_region $route $topleft_x $topleft_y $bottomright_x $bottomright_y node]
    dict set SB_North_nets_node $net $node

    if {$idx != -1} {
      set new_route [remove_branch2 $route $node]
      dict set nets_route $net $new_route
    }
    incr i 1
  }

  if {[dict size $nets_route] != 128} {logger.error "Expected 128 items."}

  set i 1
  set size [dict size $nets_route]
  dict for {net route} $nets_route {
    #logger.debug "($i/$size) Net: $net"
    set routestring [get_route_string_from_route_list $route]
    set actualroute [get_property ROUTE [get_nets $net]]
    set_property ROUTE {} [get_nets $net]
    set_property ROUTE $routestring [get_nets $net]
    incr i 0
  }
}


open_checkpoint "/home/jeroen/git/Merge/drt/examples/casestudy-aes/config1/input/modules/module_SubBytes_config1.dcp"
remove_SB_North_branch
write_checkpoint "/home/jeroen/temp/module_SubBytes_config1.dcp" -force
close_project