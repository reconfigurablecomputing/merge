##########################################################################################
# Merge of a module into the partition of the static system
#
# Run this in Vivado with:
#   source run_merge.tcl
# or
#   paste into TCL Console of Vivado
#
##########################################################################################
package require Tcl 8.0
package require struct::set
package require struct::list


##########################################################################################
# Script parameters:
##########################################################################################
set path_to_script            [file dirname [file normalize [info script]]]
set path_to_project           $path_to_script
set log_dir                   "$path_to_project/log"
set path_input                "${path_to_project}/input"
set path_output               "${path_to_project}/output"
set modules_input             "${path_input}/modules"
set modules_output            "${path_output}/modules"
set static_system_input       "${path_input}/staticsystem"
set static_system_output      "${path_output}/staticsystem"
set script_mapping            "mapping.tclconfig"
set inst_partialarea          "inst_PartialArea"
set inst_module               "inst_Module"
set inst_connectionprimitive  "inst_ConnectionPrimitive"
##########################################################################################


##########################################################################################
# Some helper procs
##########################################################################################
# Define a restart command for easy debugging (command 'restart' is already used for the simulator, hence rs)
proc rs {{arg {}}} {
  if {[current_project -quiet] != ""} { close_design }

  set path_to_script            [file dirname [file normalize [info script]]]
  set path_to_project           $path_to_script
  set log_dir                   "$path_to_project/log"

  # Clean up the workspace
  if {$arg == "c"} {
    cd $path_to_script
    lappend files {*}[glob -nocomplain Output/Modules/*.dcp]
    lappend files {*}[glob -nocomplain Output/StaticSystem/*.dcp]
    lappend files {*}[glob -nocomplain *_saved.tclvar]
    lappend files {*}[glob -nocomplain saved/*_saved.tclvar]
    lappend files {*}[glob -nocomplain hs_err_*.log]
    lappend files $log_dir/logger.log
    if {[llength $files] > 0} {file delete {*}$files}
  }
  source $path_to_script/run_merge.tcl
}
##########################################################################################


##########################################################################################
# internal variables
set static_system_input_dcp [lindex [glob "${static_system_input}/*.dcp"] 0]
set static_system_name  [string map {".dcp" ""} [file tail $static_system_input_dcp]]
set modules_dcp [glob "${modules_input}/*.dcp"]
##########################################################################################


##########################################################################################
# Start
##########################################################################################
cd $path_to_project
exec mkdir -p $log_dir
source list_extensions.tcl
source dict_extensions.tcl
source utils_debug.tcl
source utils.tcl
source utils_vivado.tcl
source utils_net_route.tcl
source partition_nets.tcl
file delete $log_dir/logger.log
logger.info "============================================================================"
logger.info "Start of script [file tail [info script]]."
if {[version -short] != "2018.3"} { logger.warning "Untested in this version of Vivado: [version -short]"}
set_param messaging.defaultLimit 2000
set_param tcl.collectionResultDisplayLimit 0
partition_nets::create 1


##########################################################################################
# Read the interface mapping from file
##########################################################################################
set script "$path_input/$script_mapping"
unset -nocomplain mapping_config
unset -nocomplain modules_config

if {[file exist $script]==0} {logging.error "Error: Mapping script not found: ${script}"}
source $script
if {![info exists mapping_config]}   {logging.error "Error: Variable 'mapping_config' not set"}
if {![info exists modules_config]}   {logging.error "Error: Variable 'modules_config' not set"}
if {[dict size $mapping_config] < 1} {logging.error "Error: Variable 'mapping_config' is empty"}
if {[llength $modules_config] < 1}   {logging.error "Error: Variable 'modules_config' is empty"}
set interface_map [dict merge [dict create] $mapping_config]
set modules $modules_config

# Specify all partition nets not used per configuration
if {![info exists unused_p2s]} {set unused_p2s [list]}
if {![info exists unused_sp2]} {set unused_s2p [list]}

##########################################################################################

open_checkpoint $static_system_input_dcp

##########################################################################################
# Preserve anchor logic, before placing modules
##########################################################################################
source anchor_logic.tcl
anchor_logic::get_anchor_logic $inst_partialarea
slvar::save anchor_logic::v::anchor_luts
slvar::save anchor_logic::v::anchor_luts_pins_gnd
slvar::save anchor_logic::v::anchor_luts_nets
slvar::save anchor_logic::v::anchor_routes
set lut_pin_assignments [get_lut_pin_assignments]
slvar::save lut_pin_assignments
##########################################################################################


##########################################################################################
# Prepare the modules for placing
# - get the ports from the partial area
# - place them in the netlist of the modules
# - assign the LOCK_PINS property for each LUT cell
##########################################################################################
set virtual_ports [partition_nets::get_virtual_ports $static_system_input_dcp $inst_partialarea]
partition_nets::add_virtual_ports_and_lock_pin_constraints $modules $inst_module $modules_input $modules_output $virtual_ports

if {[current_project -quiet] == ""} { open_checkpoint $static_system_input_dcp }

##########################################################################################
# Prepare the static design for module placement
##########################################################################################
set pblock [get_pblocks -of_objects [get_cells $inst_partialarea]]
set_property PARENT ROOT $pblock
set cell [get_cells -of_objects $pblock]
set nets [get_nets -of_objects $cell -filter "TYPE !~ *CLOCK"]
set_property IS_ROUTE_FIXED 0 [get_nets -hierarchical]
set_property IS_ROUTE_FIXED 1 $nets
set_property HD.partition 1 $cell
update_design -cells $cell -black_box
##########################################################################################


##########################################################################################
# Place the module into the static design
##########################################################################################
set modules_dcp [glob "${modules_output}/*.dcp"]
partition_nets::place_modules $modules $modules_dcp
##########################################################################################

##########################################################################################
# Reconnect all nets
##########################################################################################
write_checkpoint -force "${static_system_output}/${static_system_name}_modules_placed.dcp"
partition_nets::reconnect_s2p_static_nets interface_map $inst_partialarea $inst_connectionprimitive $inst_module
partition_nets::reconnect_p2s_static_nets interface_map $inst_partialarea $inst_connectionprimitive $inst_module
##########################################################################################

# Save intermediate design
save_checkpoint "${static_system_output}/${static_system_name}_nets_routed.dcp"

##########################################################################################
# Remove non connected nets
##########################################################################################
set nets [get_nets -of_objects [get_cells $inst_partialarea]]
set_property DONT_TOUCH 0 [get_nets $nets]
remove_cell $inst_partialarea

# Specify all partition nets not used per configuration
# if {![info exists unused_p2s]} {set unused_p2s [list]}
# if {![info exists unused_sp2]} {set unused_s2p [list]}

if {$modules == "MIX_COLUMNS_LsMLsM SUBBYTES_MsLBsM_0s1_W_NE SHIFT_ROWS_LsMLsM_1s1"} {
  remove_net SHIFT_ROWS_LsMLsM_1s1/i_p2s
  remove_cells_from_pblock SHIFT_ROWS_LsMLsM_1s1_pb_inst_ConnectionPrimitiveEastOutput [get_cells [list {SHIFT_ROWS_LsMLsM_1s1/inst_ConnectionPrimitiveEastOutput}]]
  remove_cells_from_pblock SHIFT_ROWS_LsMLsM_1s1_pb_inst_ConnectionPrimitiveNorthOutput [get_cells [list {SHIFT_ROWS_LsMLsM_1s1/inst_ConnectionPrimitiveNorthOutput}]]
  remove_cells_from_pblock SHIFT_ROWS_LsMLsM_1s1_pb_inst_ConnectionPrimitiveWestInput [get_cells [list {SHIFT_ROWS_LsMLsM_1s1/inst_ConnectionPrimitiveWestInput}]]
  remove_cell [list {SHIFT_ROWS_LsMLsM_1s1/inst_ConnectionPrimitiveEastOutput} {SHIFT_ROWS_LsMLsM_1s1/inst_ConnectionPrimitiveNorthOutput} {SHIFT_ROWS_LsMLsM_1s1/inst_ConnectionPrimitiveWestInput}]
  delete_pblock [get_pblocks  SHIFT_ROWS_LsMLsM_1s1_pb_inst_ConnectionPrimitiveWestInput]
  delete_pblock [get_pblocks  SHIFT_ROWS_LsMLsM_1s1_pb_inst_ConnectionPrimitiveEastOutput]
  delete_pblock [get_pblocks  SHIFT_ROWS_LsMLsM_1s1_pb_inst_ConnectionPrimitiveNorthOutput]
  remove_cells_from_pblock MIX_COLUMNS_LsMLsM_pb_inst_ConnectionPrimitiveSouthInput [get_cells [list {MIX_COLUMNS_LsMLsM/inst_ConnectionPrimitiveSouthInput}]]
  remove_cell [list {MIX_COLUMNS_LsMLsM/inst_ConnectionPrimitiveSouthInput}]
  remove_net SHIFT_ROWS_LsMLsM_1s1/i_s2p
  set_property DONT_TOUCH 0 [get_nets SHIFT_ROWS_LsMLsM_1s1/inst_Module/SR_IN[*]]
  remove_net SHIFT_ROWS_LsMLsM_1s1/inst_Module/SR_IN
  remove_net [list {SHIFT_ROWS_LsMLsM_1s1/blocker_net_BlockSelection} {SHIFT_ROWS_LsMLsM_1s1/blocker_net_BlockSelection}]
  remove_cells_from_pblock pb_SHIFT_ROWS_LsMLsM_1s1 [get_cells [list {SHIFT_ROWS_LsMLsM_1s1/gnd_for_BlockSelection}]]
  remove_cell [list {SHIFT_ROWS_LsMLsM_1s1/gnd_for_BlockSelection}]
}

# Save intermediate design
save_checkpoint "${static_system_output}/${static_system_name}_removed_connection_primitives.dcp"

# Handle all p2s nets that are not connected
connect_nets_to_gnd $unused_p2s

# Remove unused s2p nets
remove_antenna_branch_nets $unused_s2p

# Restore proxy logic
anchor_logic::restore_s2p_anchor_LUTs $unused_s2p $inst_partialarea

# Restore design constraints
partition_nets::restore_design_constraints $inst_partialarea
save_checkpoint "${static_system_output}/${static_system_name}_lockpins_restored.dcp"

# Restore clock logic
if {[info exists internal_module_clk] && [info exists external_module_clk]} {
  partition_nets::restore_clock_nets $modules $inst_module $internal_module_clk $external_module_clk
  save_checkpoint "${static_system_output}/${static_system_name}_clocknets.dcp"
}


# Fix all nets to prevent route optimizations
set_property IS_ROUTE_FIXED 1 [get_nets -hierarchical]
# We move some clock pins to another net and delete some flipflops cells.
# Do we get antenna nets, if yes either remove this manually or let Vivado route_design command resolve this.
# Is a intermediate route_design command required?
# not for example1b
#route_design -physical_nets

if {$modules == "MIX_COLUMNS_LsMLsM SUBBYTES_MsLBsM_0s1_W_NE SHIFT_ROWS_LsMLsM_1s1"} {
  clear_route [get_nets inst_AES/AESRound/x1y0_p2s_e[*]]
  clear_route [get_nets inst_AES/AESRound/x1y0_p2s_e[*]]
  clear_route [get_nets inst_AES/AESRound/x1y1_p2s_e[*]]

  # When a module turns into a blackbox we must delete it, else it will be a DRC error
  logger.todo "When a module turns into a blackbox we must delete it, else it will be a DRC error"
  remove_cells_from_pblock SHIFT_ROWS_LsMLsM_1s1_pb_inst_Module [get_cells [list {SHIFT_ROWS_LsMLsM_1s1}]]
  remove_cell [list {SHIFT_ROWS_LsMLsM_1s1}]

  # This seems to be required before writing the bitstream, which is strange. Looks more like a bug.
  # Reading the IOSTANDARD from the ports already returns LVCMOS18
  # Suggest: Save and restore these properties
  set_property IOSTANDARD LVCMOS18 [get_ports]
}

save_checkpoint "${static_system_output}/${static_system_name}_after_postfix.dcp"

# Merging nets issue, a net alias is required for some nets.
# In Vivado when we select a net in the Device view, there is an Aliases tab in the Net Properties window.
# It show all the nets that have the same route and pins.
# Via TCL we can get this info via the command get_nets -segments.
# At this point we do not know how to create such alias from TCL.
# So for now the only option is to clear the ROUTE property of the net.
if {$modules != "MIX_COLUMNS_LsMLsM SUBBYTES_MsLBsM_0s1_W_NE SHIFT_ROWS_LsMLsM_1s1"} {
  set nets_conflicts [get_nets * -filter {ROUTE_STATUS==CONFLICTS && NAME=~*p2s*}]
  foreach net $nets_conflicts {
    set_property ROUTE {} [get_nets $net]
    logger.warning "Net '$net' has status CONFLICTS and its route has been cleared."
  }
}


# Do a final route of the design
# Certain operations on nets and logic mark some nets as unrouted
route_design

# Write the final checkpoint and bitstream
set dcp "${static_system_output}/${static_system_name}_final.dcp"
save_checkpoint $dcp

set bitfile [string map {".dcp" ".bit"} $dcp]
write_bitstream -force $bitfile

logger.info "============================================================================"
logger.info "End of script [file tail [info script]]."
