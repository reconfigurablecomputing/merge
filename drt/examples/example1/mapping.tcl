# Define a list of modules
set modules [list module1]

# Define the interface mapping
set mapping {
  {x0y0_s2p_w[*]} {module1/inst_ConnectionPrimitiveWestInput/x0y0_s2p_w[*]}
  {module1/inst_ConnectionPrimitiveWestOutput/x0y0_p2s_w[*]} {x0y0_p2s_w[*]}
}

# Define the unused s2p and p2s nets
set unused_s2p [list]
set unused_p2s [list]

# Define the clock net of the module instance
set internal_module_clk "clk"

# Define the clock net outside the partial area of the module
set external_module_clk "fclk_clk0"

# Assign the config, do not change
set modules_config [list {*}$modules]
set mapping_config $mapping