##########################################################################################
# Design reconstruction for example1
#
# In the Vivado console, go to the folder where this script resides and source it:
#   cd /home/jeroen/git/Merge/drt/examples/example1
#   source example1.tcl
##########################################################################################

set path_to_this_script  [file dirname [file normalize [info script]]]
set drt_script           "/home/jeroen/git/Merge/drt/drt.tcl"
set modules_input        "${path_to_this_script}/input/modules"
set modules_output       "${path_to_this_script}/output/modules"
set static_input         "${path_to_this_script}/input/staticsystem"
set static_output        "${path_to_this_script}/output/staticsystem"
set mapping_file         "${path_to_this_script}/mapping.tcl"

proc rs {} {
  cd /home/jeroen/git/Merge/drt/examples/example1
  source example1.tcl
}

source $drt_script
drt::merge $modules_input $modules_output $static_input $static_output $mapping_file
