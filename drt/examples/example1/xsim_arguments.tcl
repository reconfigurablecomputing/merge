# use this file in conjuction with this:
# exec xsim add_timesim -gui -tclbatch {sim_arg.tcl}
# this adds all signals to the wave diagram:
# add_wave -r /

add_wave {{/top_lib_work/clk}}
add_wave {{/top_lib_work/reset}}
add_wave {{/top_lib_work/module1/inst_Module/a}}
add_wave {{/top_lib_work/module1/inst_Module/b}}
add_wave {{/top_lib_work/module1/inst_Module/o}}

# 10MHz
add_force {/top_lib_work/clk} -radix hex {1} {0 50000ps} -repeat_every 100000ps

add_force {/top_lib_work/reset} -radix unsigned {1 0ns}
add_force {/top_lib_work/inp} -radix unsigned {1 0ns}
add_force {/top_lib_work/btn} -radix hex {0 0ns}

run 100ns
add_force {/top_lib_work/reset} -radix unsigned {0 0ns}
run 500ns

# 100MHz
add_force {/top_lib_work/clk} -radix hex {1} {0 5000ps} -repeat_every 10000ps
run 100ns

