# AES Configuration 2
set modules_AES_config_2 [list module_MixColumns_config2 module_SubBytes_config2 module_ShiftRows_config2]

# Use {} or CLEAR_ROUTE

set mapping_AES_config_2 {
  {inst_AES/AESRound/x0y0_s2p_w[*]} {module_SubBytes_config2/inst_ConnectionPrimitiveWestInput/x0y0_s2p_w[*]}
  {inst_AES/AESRound/x0y1_s2p_w[*]} {CLEAR_ROUTE}
  {inst_AES/AESRound/x0y0_s2p_n[*]} {CLEAR_ROUTE}
  {inst_AES/AESRound/x1y0_s2p_n[*]} {CLEAR_ROUTE}
  {inst_AES/AESRound/x1y0_s2p_e[*]} {CLEAR_ROUTE}
  {inst_AES/AESRound/x1y1_s2p_e[*]} {CLEAR_ROUTE}
  {module_SubBytes_config2/inst_ConnectionPrimitiveEastOutput/x0y0_p2s_e[*]} {module_ShiftRows_config2/inst_ConnectionPrimitiveWestInput/x0y0_s2p_w[*]}
  {module_ShiftRows_config2/inst_ConnectionPrimitiveEastOutput/x0y0_p2s_e[*]} {inst_AES/AESRound/x1y0_p2s_e[*]}
  {module_ShiftRows_config2/inst_ConnectionPrimitiveSouthOutput/x0y0_p2s_s[*]} {module_MixColumns_config2/inst_ConnectionPrimitiveNorthInput/x1y0_s2p_n[*]}
  {module_MixColumns_config2/inst_ConnectionPrimitiveWestOutput/x0y0_p2s_w[*]} {inst_AES/AESRound/x0y1_p2s_w[*]}
}

set modules_config [list {*}$modules_AES_config_2]
set mapping_config $mapping_AES_config_2
set inst_partialarea "inst_AES/AESRound/inst_PartialArea"
set unused_s2p {}
set unused_p2s {inst_AES/AESRound/x0y0_p2s_w[*] inst_AES/AESRound/x0y0_p2s_n[*] inst_AES/AESRound/x1y1_p2s_e[*] inst_AES/AESRound/x1y0_p2s_n[*]}

