##########################################################################################
# case study AES
#
# In the Vivado console:
#   cd /home/jeroen/git/Merge/drt/examples/casestudy-aes
#   source casestudy-aes-config2.tcl
##########################################################################################

set path_to_this_script  [file dirname [file normalize [info script]]]
set drt_script           "/home/jeroen/git/Merge/drt/drt.tcl"
set modules_input        "${path_to_this_script}/input/modules"
set modules_output       "${path_to_this_script}/output/modules"
set static_input         "${path_to_this_script}/input/staticsystem"
set static_output        "${path_to_this_script}/output/staticsystem"
set mapping_file         "${path_to_this_script}/mapping-config2.tcl"

proc rs {} {
  cd /home/jeroen/git/Merge/drt/examples/casestudy-aes
  source casestudy-aes-config2.tcl
}

source $drt_script
drt::merge $modules_input $modules_output $static_input $static_output $mapping_file
