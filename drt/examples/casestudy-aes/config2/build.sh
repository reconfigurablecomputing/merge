#!/bin/bash

# clean up
rm -f output/modules/*.dcp
rm -f output/staticsystem/*.dcp
rm -f output/staticsystem/*.bit
rm -f -r output/staticsystem/.saved
rm -f -r output/staticsystem/.log
rm -f -r .Xil

vivado -mode batch -nojournal -nolog -source casestudy-aes-config2.tcl

