# This seems to be required before writing the bitstream, which is strange. Looks more like a bug.
# Reading the IOSTANDARD from the ports already returns LVCMOS18
# Suggest: Save and restore these properties
set_property IOSTANDARD LVCMOS18 [get_ports]