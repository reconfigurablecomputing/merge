##########################################################################################
# Design reconstruction simulation
#
# Run this in vivado with:
#   source run_simulation.tcl
# or
#   paste into TCL Console
#
##########################################################################################


##########################################################################################
# Script parameters:
##########################################################################################
set path_to_vivado          "/home/jeroen/Xilinx/Vivado/2018.3"
set path_to_script          [file dirname [file normalize [info script]]]
set path_to_drt_sim_script  "/home/jeroen/git/Merge/drt/drt_sim.tcl"
set top_unitname            "top_lib_work"
##########################################################################################

cd $path_to_script
source $path_to_drt_sim_script

# Path of the DCP file
set dcp "${path_to_script}/output/staticsystem/staticsystem_final.dcp"

# Path the xsim arguments file
set xsim_arg "${path_to_script}/xsim_arguments.tcl"

drt_sim::create $path_to_vivado
drt_sim::run $dcp $xsim_arg $top_unitname
