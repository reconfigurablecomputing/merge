# use this file in conjunction with this:
# exec xsim add_timesim -gui -tclbatch {sim_arg.tcl}
# this adds all signals to the wave diagram:
# add_wave -r /

add_wave {{/top_lib_work/CLK}}
add_wave {{/top_lib_work/inst_Clock/clk_out1}}
add_wave {{/top_lib_work/BUTTONS}}
add_wave {{/top_lib_work/DATA_AVAIL}}
add_wave {{/top_lib_work/DATA_READY}}
add_wave {{/top_lib_work/DPR_SEL}} -radix dec
add_wave {{/top_lib_work/LEDS}}

#add_wave {{/top_lib_work/inst_AES/DATA_OUT}}
#add_wave {{/top_lib_work/inst_AES/DATA_IN}}
#add_wave {{/top_lib_work/NLW_inst_AES_DATA_IN_UNCONNECTED}}
#add_wave {{/top_lib_work/NLW_inst_AES_KEY_UNCONNECTED}}
#add_wave {{/top_lib_work/inst_Clock/clk_out1}}

# 10MHz clock
# add_force {/top_lib_work/CLK} -radix hex {1} {0 50000ps} -repeat_every 100000ps
# 100MHz clock
add_force {/top_lib_work/CLK} -radix hex {1} {0 5000ps} -repeat_every 10000ps
add_force {/top_lib_work/NLW_inst_AES_DATA_IN_UNCONNECTED} -radix hex {3243F6A8885A308D313198A2E037073 0ns}
add_force {/top_lib_work/NLW_inst_AES_KEY_UNCONNECTED} -radix hex {2B7E151628AED2A6ABF7158809CF4F3C 0ns}
add_force {/top_lib_work/DATA_AVAIL} -radix unsigned {0 0ns}
add_force {/top_lib_work/inst_AES/RESET} -radix dec {0 0ns}

add_force {/top_lib_work/DPR_SEL} -radix dec {43 0ns}
add_force {/top_lib_work/BUTTONS} -radix bin {0001 0ns}
run 200ns
add_force {/top_lib_work/DATA_AVAIL} -radix unsigned {1 0ns}
run 2000ns
add_force {/top_lib_work/DATA_AVAIL} -radix unsigned {0 0ns}
add_force {/top_lib_work/BUTTONS} -radix bin {0110 0ns}
run 200ns
add_force {/top_lib_work/DATA_AVAIL} -radix unsigned {1 0ns}
run 2000ns