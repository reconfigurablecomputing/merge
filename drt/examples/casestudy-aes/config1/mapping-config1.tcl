# AES Configuration 1
set modules_AES_config_1 [list module3 module1 module2]

# Use {} or CLEAR_ROUTE to clear the routing of certain unused nets.
# Use REMOVE_INTERFACE x31y133:x40y50
#  where the x-y coordinates is the complete rectangular region of the connection primitive including the routing

set mapping_AES_config_1 {
  {inst_AES/AESRound/x0y1_s2p_w[*]} {module1/inst_ConnectionPrimitiveWestInput/x0y0_s2p_w[*]}
  {inst_AES/AESRound/x0y0_s2p_w[*]} {CLEAR_ROUTE}
  {inst_AES/AESRound/x0y0_s2p_n[*]} {CLEAR_ROUTE}
  {inst_AES/AESRound/x1y0_s2p_n[*]} {CLEAR_ROUTE}
  {inst_AES/AESRound/x1y0_s2p_e[*]} {CLEAR_ROUTE}
  {inst_AES/AESRound/x1y1_s2p_e[*]} {CLEAR_ROUTE}
  {module1/inst_ConnectionPrimitiveNorthOutput/x0y0_p2s_n[*]} {REMOVE_INTERFACE x31y133:x40y50}
  {module1/inst_ConnectionPrimitiveEastOutput/x0y0_p2s_e[*]} {module2/inst_ConnectionPrimitiveWestInput/x0y0_s2p_w[*]}
  {module2/inst_ConnectionPrimitiveEastOutput/x0y0_p2s_e[*]} {inst_AES/AESRound/x1y1_p2s_e[*]}
  {module2/inst_ConnectionPrimitiveNorthOutput/x0y0_p2s_n[*]} {module3/inst_ConnectionPrimitiveSouthInput/x0y0_s2p_s[*]}
  {module3/inst_ConnectionPrimitiveEastOutput/x0y0_p2s_e[*]} {inst_AES/AESRound/x1y0_p2s_e[*]}
}

set modules_config [list {*}$modules_AES_config_1]
set mapping_config $mapping_AES_config_1
set inst_partialarea "inst_AES/AESRound/inst_PartialArea"
set unused_s2p {inst_AES/AESRound/inst_PartialArea/x0y0_s2p_w[*]}
set unused_p2s {inst_AES/AESRound/x0y0_p2s_w[*] inst_AES/AESRound/x0y0_p2s_n[*] inst_AES/AESRound/x0y1_p2s_w[*] inst_AES/AESRound/x1y0_p2s_n[*]}

