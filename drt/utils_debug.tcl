set debug_level 1
set log_level 1

proc debug_trace {text} {
  set systemTime [clock seconds]
  set datetime [clock format $systemTime -format %H:%M:%S]
  set scriptname [info script]
  global log_dir

  # open file for appending
  set fp [open $log_dir/debug.log a]
  puts $fp "${datetime} ${scriptname} - debug: ${text}"
  puts "debug_trace ${datetime} - debug: ${text}"
  close $fp
}

proc debug_var {varname varvalue} {
  set systemTime [clock seconds]
  set datetime [clock format $systemTime -format %H:%M:%S]
  set scriptname [info script]
  global log_dir

  # open file for appending
  set fp [open $log_dir/debug_var.log a]
  puts $fp "set ${varname} { ${varvalue} }"
  puts $fp ""
  close $fp
}

########################################################################################
# breakpoint
# https://wiki.tcl-lang.org/page/A+minimal+debugger
# Note: does not work in the TCL Console of Vivado.
# Only when Vivado is started from terminal, you are able to give command via stdin, o
# otherwise the script hangs and no input is possible
#
# Arguments:
#
# Result:
#
########################################################################################
proc bp {{s {}}} {
  if {![info exists ::bp_skip]} {
      set ::bp_skip [list]
  } elseif {[lsearch -exact $::bp_skip $s]>=0} return
  if [catch {info level -1} who] {set who ::}
  while 1 {
    puts -nonewline "$who/$s> "; flush stdout
    gets stdin line
    if {[expr { $line eq "c"}]} {puts "continuing.."; break}
    if {[expr { $line eq "i"}]} {set line "info locals"}
    catch {uplevel 1 $line} res
    puts $res
  }
}

########################################################################################
# todo
# a place in code where some work has to be done
# Arguments:
#
# Result:
#
########################################################################################
proc todo {{message {}} } {
  set where [lindex [info level 1] 0]
  set msg "@'${where}': $message"
  logger.todo $msg
}


########################################################################################
# break here
# stop execution of the script with an error statement
# Arguments:
#
# Result:
#
########################################################################################
proc break_here args {
  foreach arg $args {
    if {$arg == "c"} { close_design }
    if {$arg == "l"} {
      puts "Break here encountered, locals:"
      uplevel 1 {
        foreach loc [info locals] { puts "$loc = '[set $loc]'" }
        unset loc
      }
    } else {
      upvar $arg var
      puts "$arg = '$var'"
    }
  }
  error "Break here."
}

########################################################################################
# log locals
#
# Arguments:
#
# Result:
#
########################################################################################
proc log_locals {} {
  global log_level
  if {$log_level==1} {
    uplevel 1 {
      set log_data [list "log_locals:"]
      foreach loc [info locals] {
        if {$loc != "log_data"} { lappend log_data  "$loc = '[set $loc]'"}
      }
      logger.log $log_data
      unset loc
      unset log_data
    }
  }
}


########################################################################################
# puts debug
#
# Arguments:
#
# Result:
#
########################################################################################
proc putsd {msg} {
  global debug_level
  if {$debug_level==1} {
    puts $msg
    logger.debug $msg
  }
}


########################################################################################
# warning
# give a orange warning in the TCL console
# Arguments:
#
# Result:
#
########################################################################################
proc warning {message {where {}}} {logger.warning $message}


########################################################################################
# local var to global scope
#
# Arguments:
#
# Result:
#
########################################################################################
proc var_to_global {upvar_var} {
  upvar $upvar_var var_value
  global ::$upvar_var
  set $upvar_var $var_value
}