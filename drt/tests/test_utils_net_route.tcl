#!/usr/bin/tclsh
package require Tcl 8.0
package require struct::set
package require struct::list

set path_to_script            [file dirname [file normalize [info script]]]
set log_dir                   "$path_to_script/log"

file mkdir "$path_to_script/log"

source assert.tcl
source list_extensions.tcl
source utils_debug.tcl
source utils_net_route.tcl

proc test_remove_branch {} {
  set test_nodes1 [list \{ n1 n2 n3 N4 n5 \}]
  set test_nodes2 [list \{ n1 \{ n2 n3 N4 \} n5 n6 n7 \}]
  set test_nodes3 [list \{ n1 \{ n2 n3 n4 \} n5 n6 N7 \}]
  set test_nodes4 [list \{ n1 \{ n2 n3 n4 \{ n5 n6 N7 \} \} n8 n9 n10 \}]
  set test_nodes5 [list \{ n1 \{ \{ n2 n3 n4 \} n5 n6 N7 \} n8 n9 n10 \}]
  set test_nodes6 [list \{ n1 \{ n2 \{ n3 n4 n5 \} n6 n7 N8 \} n9 n10 n11 \}]
  set test_nodes7 [list \{ n1 n2 n3 \{ n4 n5 n6 \} n7 n8 \{ n9 \{ n10 n11 n12 \} n13 n14 N15 \} \}]
  set test_nodes8 [list \{ n1 n2 n3 \{ n4 n5 n6 \} n7 n8 \{ n9 \{ n10 n11 n12 \} n13 N14 n15 \} \}]

  set test_nodes1_ref [list \{ \}]
  set test_nodes2_ref [list \{ n1 n5 n6 n7 \}]
  set test_nodes3_ref [list \{ n1 n2 n3 n4 \}]
  set test_nodes4_ref [list \{ n1 \{ n2 n3 n4 \} n8 n9 n10 \}]
  set test_nodes5_ref [list \{ n1 \{ n2 n3 n4 \} n8 n9 n10 \}]
  set test_nodes6_ref [list \{ n1 \{ n2 n3 n4 n5 \} n9 n10 n11 \}]
  set test_nodes7_ref [list \{ n1 n2 n3 \{ n4 n5 n6 \} n7 n8 n9 n10 n11 n12 \}]
  set test_nodes8_ref [list \{ n1 n2 n3 \{ n4 n5 n6 \} n7 n8 n9 n10 n11 n12 \}]

  set test_nodes1a_result [remove_branch $test_nodes1 N4]
  set test_nodes1b_result [remove_branch $test_nodes1 n5]
  set test_nodes2_result [remove_branch $test_nodes2 N4]
  set test_nodes3_result [remove_branch $test_nodes3 N7]
  set test_nodes4_result [remove_branch $test_nodes4 N7]
  set test_nodes5_result [remove_branch $test_nodes5 N7]
  set test_nodes6_result [remove_branch $test_nodes6 N8]
  set test_nodes7_result [remove_branch $test_nodes7 N15]
  set test_nodes8_result [remove_branch $test_nodes8 N14]

  assert_eq $test_nodes1_ref $test_nodes1a_result
  assert_eq $test_nodes1_ref $test_nodes1b_result
  assert_eq $test_nodes2_ref $test_nodes2_result
  assert_eq $test_nodes3_ref $test_nodes3_result
  assert_eq $test_nodes4_ref $test_nodes4_result
  assert_eq $test_nodes5_ref $test_nodes5_result
  assert_eq $test_nodes6_ref $test_nodes6_result
  assert_eq $test_nodes7_ref $test_nodes7_result
  assert_eq $test_nodes8_ref $test_nodes8_result
}

test_remove_branch