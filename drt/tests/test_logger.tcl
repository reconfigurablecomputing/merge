#!/usr/bin/tclsh

set path_to_script            [file dirname [file normalize [info script]]]
set log_dir                   "$path_to_script/log"

file mkdir "$path_to_script/log"

source logger.tcl

proc test_logger {} {
  logger.log "Some log text"
  logger.info "Some info text"
  logger.trace "Some trace text"
  logger.debug "Some debug text"
  logger.warning "Some warning text"

  set list1 [list 1 2 3 4]
  logger.debug "Some data to log: '$list1'"

  logger.todo "Some work to be done here"

  # should log and throw an error
  logger.error "Some error text"
}

test_logger