#!/usr/bin/tclsh
package require Tcl 8.0
package require struct::set
package require struct::list

set path_to_script            [file dirname [file normalize [info script]]]
set log_dir                   "$path_to_script/log"
file mkdir "$path_to_script/log"


source assert.tcl
source utils_debug.tcl

proc test_putsd {} {
    putsd "Test"
}

proc test_todo {} {
  todo "Do some programming here."
}

proc test_log_locals1 {} {
 set x 3
 set ls1 [list 1 2 3 4]

 log_locals
}

proc test_log_locals2 {} {
 puts "test_log_locals2"
 log_locals
}

proc test_break_here {} {
 set x 3
 set ls1 [list 4 3 2 1 0]

 break_here l
}


proc for_testing_var_to_global {} {
 set x 3
 set ls1 [list 5 6 7 8]
 var_to_global ls1
}

proc test_var_to_global {} {
 set x 3
 set ls1 [list 1 2 3 4]

 for_testing_var_to_global
 puts "Local ls1: $ls1"
}

# Execute without errors:
test_putsd
test_todo
test_log_locals1
test_log_locals2
test_var_to_global

# Should be equal
assert_eq $ls1 [list 5 6 7 8]

# Execute with error:
test_break_here
