#!vivado -source /home/jeroen/Xilinx/Projects/2018.3/zedboard/PRMergeGS/Merge/test_restore_anchor_logic.tcl
##########################################################################################
# test_restore_anchor_logic
##########################################################################################
package require Tcl 8.0
package require struct::set
package require struct::list


##########################################################################################
# Script parameters:
##########################################################################################
set path_to_project           "/home/jeroen/Xilinx/Projects/2018.3/zedboard/PRMergeGS/Merge"
set log_dir                   "$path_to_project/log"
set path_input                "${path_to_project}/Input"
set path_output               "${path_to_project}/Output"
set modules_input             "${path_input}/Modules"
set modules_output            "${path_output}/Modules"
set static_system_input       "${path_input}/StaticSystem"
set static_system_output      "${path_output}/StaticSystem"
set script_mapping            "mapping.tclconfig"
set inst_partialarea          "inst_PartialArea"
set inst_module               "inst_Module"
set inst_connectionprimitive  "inst_ConnectionPrimitive"
##########################################################################################


##########################################################################################
# Some helper procs
##########################################################################################
# Define a restart command for easy debugging (command 'restart' is used for the simulator)
proc rs {{arg {}}} {
  if {[current_project -quiet] != ""} { close_design }

  # Clean up the workspace
  if {$arg == "c"} {
    # Use absolute path only here.
    cd "/home/jeroen/Xilinx/Projects/2018.3/zedboard/PRMergeGS/Merge"
    lappend files {*}[glob -nocomplain *_saved.tclvar]
    lappend files {*}[glob -nocomplain hs_err_*.log]
    lappend files /home/jeroen/Xilinx/Projects/2018.3/zedboard/PRMergeGS/Merge/log/logger.log
    if {[llength $files] > 0} {file delete {*}$files}
  }
  source "/home/jeroen/Xilinx/Projects/2018.3/zedboard/PRMergeGS/Merge/test_restore_anchor_logic.tcl"
}

# Save and open the saved checkpoint
proc save_checkpoint {dcp} {
  write_checkpoint -force $dcp
  close_design
  open_checkpoint $dcp
}
##########################################################################################


##########################################################################################
# internal variables
set static_system_input_dcp [lindex [glob "${static_system_input}/*.dcp"] 0]
set static_system_name  [string map {".dcp" ""} [file tail $static_system_input_dcp]]
set modules_dcp [glob "${modules_input}/*.dcp"]
##########################################################################################


##########################################################################################
# Start
##########################################################################################
cd $path_to_project
exec mkdir -p $log_dir
source assert.tcl
source list_extensions.tcl
source dict_extensions.tcl
source utils_debug.tcl
source utils.tcl
source utils_net_route.tcl
source partition_nets.tcl

logger.info "===================================================================================================="
logger.info "Start of script [file tail [info script]]."
open_checkpoint $static_system_input_dcp

##########################################################################################
# Preserve anchor logic, before placing modules
##########################################################################################
source anchor_logic.tcl
anchor_logic::get_anchor_logic $inst_partialarea
slvar::save anchor_logic::v::anchor_luts
slvar::save anchor_logic::v::anchor_luts_pins_gnd
slvar::save anchor_logic::v::anchor_luts_nets
slvar::save anchor_logic::v::anchor_routes

set lut_pin_assignments [dict create]
set pins [get_pins -of_object [get_cells -hierarchical -filter { PRIMITIVE_TYPE =~ LUT.*.* }]]
foreach pin $pins {
  set bel_pin  [get_bel_pins -of_objects $pin]
  dict set lut_pin_assignments $pin $bel_pin
}
slvar::save lut_pin_assignments
##########################################################################################


##########################################################################################
# Prepare the static design for module placement
##########################################################################################
set pblock [get_pblocks -of_objects [get_cells $inst_partialarea]]
set_property PARENT ROOT $pblock
set cell [get_cells -of_objects $pblock]
set local_interface_nets [get_nets -of_objects $cell -filter "TYPE !~ *CLOCK"]
set_property IS_ROUTE_FIXED 0 [get_nets -hierarchical]
set_property IS_ROUTE_FIXED 1 $local_interface_nets
set_property HD.partition 1 $cell
update_design -cells $cell -black_box
##########################################################################################


set nets [get_nets -of_objects [get_cells $inst_partialarea]]
set_property HD.PARTITION 0 [get_cells $inst_partialarea]
set_property DONT_TOUCH 0 [get_cells $inst_partialarea]
set_property DONT_TOUCH 0 [get_nets $nets]
remove_cell $inst_partialarea

connect_nets_to_gnd x0y0_p2s_n[*]
connect_nets_to_gnd x0y0_p2s_w[*]
connect_nets_to_gnd x0y1_p2s_w[*]
connect_nets_to_gnd x1y0_p2s_e[*]
connect_nets_to_gnd x1y0_p2s_n[*]
connect_nets_to_gnd x1y1_p2s_e[*]

# all s2p nets
set nets {x0y0_s2p_w[*] x0y0_s2p_n[*] x0y1_s2p_w[*] x1y0_s2p_e[*] x1y0_s2p_n[*] x1y1_s2p_e[*]}
remove_antenna_branch_nets $nets

# Can not get this part to work completely:
anchor_logic::restore_s2p_anchor_LUTs $nets $inst_partialarea
# Some nets can not be routed, they have keep the yellow fly wires.
# Route string and pins look ok, maybe issue is in the BEL/LUT.

logger.debug "save dcp..."
set dcp "${static_system_output}/${static_system_name}_test_restore.dcp"
save_checkpoint $dcp
logger.debug "save dcp done."

# Route design
route_design

#Note: final design does not route.
# Nets with Routing Errors:
#  GLOBAL_LOGIC0

# Write the final checkpoint and bit stream
set dcp "${static_system_output}/${static_system_name}_final_restore.dcp"
write_checkpoint -force $dcp
close_design
open_checkpoint $dcp

set bitfile [string map {".dcp" ".bit"} $dcp]
write_bitstream -force $bitfile
