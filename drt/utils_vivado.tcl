########################################################################################
# Save and open the saved checkpoint
# Arguments:
#
# Results:
#
########################################################################################
proc save_checkpoint {dcp} {
  write_checkpoint -force $dcp
  close_design
  open_checkpoint $dcp
}


########################################################################################
# this is a way to get open dcp file
# Arguments:
#
# Results:
#
########################################################################################
proc get_open_dcp_file {} {
 set project [string map {"project_" ""} [current_project -quiet]]
 return "${project}.dcp"
}
