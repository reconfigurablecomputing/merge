
##########################################################################################
# A simple save and load method for variables from a TCL file
##########################################################################################
namespace eval slvar {

  # exports
  namespace export save
  namespace export load
  namespace export create

  # variables
  variable _dir ".saved"

  # procs

  proc create {dir} {
    set slvar::_dir $dir
  }

  ########################################################################################
  # slvar::save
  # Arguments:
  #
  # Results:
  #
  ########################################################################################
  proc save varname {
    upvar 1 $varname varvalue

    file mkdir $slvar::_dir
    set fp [open "$slvar::_dir/${varname}_saved.tclvar" w+]
    puts $fp "# Note: This file contains a saved tcl variable and can be restored using the source command."
    puts $fp "set ${varname} {${varvalue}}"
    close $fp
  }

  ########################################################################################
  # slvar::load
  # Arguments:
  #
  # Results:
  # Assigns the value to the varname.
  # Returns 1 when the saved file exist and the variable is set from that file.
  ########################################################################################
  proc load varname {
    upvar 1 $varname varvalue

    if {[file exists "$slvar::_dir/${varname}_saved.tclvar"]} {
      source "$slvar::_dir/${varname}_saved.tclvar"
      if {[info exist varname]} { set varvalue [set $varname]; return 1; }
    }
    return 0
  }

}
