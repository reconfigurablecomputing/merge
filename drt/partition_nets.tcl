########################################################################################
# Use a namespace as a data structure
########################################################################################
namespace eval partition_nets {
  namespace export reconnect_s2p_static_nets
  namespace export reconnect_p2s_static_nets
  namespace export get_virtual_ports

  variable inst_connectionprimitive "inst_ConnectionPrimitive"
  variable inst_partialarea "inst_PartialArea"
  variable enable_trace 0

  namespace eval map {
    variable s2p_static_net [dict create]
    variable p2s_static_net [dict create]
    variable s2p_module_net [dict create]
    variable p2s_module_net [dict create]
    variable s2p_connection_net [dict create]
    variable p2s_connection_net [dict create]
  }

  proc create {enable_trace} {
    variable trace_enabled
    variable inst_connectionprimitive
    variable inst_partialarea
    set trace_enabled $enable_trace
  }

  proc trace {message} {
    variable trace_enabled
    if {$trace_enabled} {logger.trace "partition_nets::$message"}
  }

  variable s2p_static_net [list]
  variable p2s_static_net [list]
  variable s2p_module_net [list]
  variable p2s_module_net [list]
  variable s2p_connection_net [list]
  variable p2s_connection_net [list]
  variable s2p_static_net_combined [list]
  variable p2s_module_net_combined [list]
  variable module_connection_nets [list]
}


proc partition_nets::place_modules {modules modules_dcp} {
  if {[current_project -quiet] == ""} { logger.error "No project is open." }

  # Iterate through all module DCP files in the folder and append them on the fabric
  foreach module $modules_dcp {
    set name [string map {".dcp" ""} [lindex [split $module "/"] end]]

    if {[lsearch $modules $name] != -1} {
      set cell $name
      set pblock "pb_${name}"

      logger.info "Place module '$module'."

      # Create a cell and pblock in which the module can reside
      create_cell $cell -black_box -reference $name
      create_pblock $pblock
      add_cells_to_pblock $pblock [get_cells $cell]

      # Read the module DCP file and place the contents in the cell
      read_checkpoint -verbose -cell $cell $module
    }
  }
}

proc partition_nets::get_virtual_ports {dcp_file inst_partialarea} {
  if {[get_open_dcp_file] != [file tail $dcp_file]} {
    if {[catch {open_checkpoint $dcp_file} res opt ]} {logger.error "Could not open DCP file: $dcp_file."}
  }

  # Get all pins of the partial area from the static system
  # These are the partition nets
  set partition_pins [get_pins -of_objects [get_cells $inst_partialarea]]
  set partition_ports [dict create]
  set partition_nets [get_nets -verbose -filter "PARENT_CELL==$inst_partialarea && MARK_DEBUG==true" -hierarchical]
  slvar::save partition_nets

  if {![slvar::load partition_ports]} {
    # Create a dict containing the pin name, direction and width.
    # The pins will be added to the module as virtual ports.
    foreach pin $partition_pins {
      set match [regexp {(^[A-Za-z0-9_\/]*)([\/])([A-Za-z0-9_]*)(\[[0-9]*\])?} $pin fullmatch inst sep thepin index]
      if {$match} {
        set direction [get_property DIRECTION [get_pins $pin]]
        lassign [get_property BUS_WIDTH [get_nets $inst_partialarea/$thepin[*]]] width
        dict append partition_ports $thepin
        dict set partition_ports $thepin [list $direction $width]
      }
    }
    slvar::save partition_ports
  }
  return $partition_ports
}

########################################################################################
# add virtual ports
#
# Arguments:
#
# Result:
#
########################################################################################
proc partition_nets::add_virtual_ports {modules modules_input modules_output virtual_ports} {
  # Check if need to add the virtual ports to the module
  set add_virtual_ports 1; # for now: 1=always
  # foreach module $modules {
  #   set dcp "${modules_output}/${module}.dcp"
  #   if {![file exist $dcp]} { set add_virtual_ports 1; break; }
  # }

  if {$add_virtual_ports} {
    # Close any open project
    if {[current_project -quiet] != ""} { close_project }

    # Add the virtual ports to each module
    foreach module $modules {
      set module_dcp "${modules_input}/${module}.dcp"
      if {![file exist $module_dcp]} { logger.warning "Warning: Input DCP file does not exist: $module_dcp."; continue; }

      # Open the dcp file
      open_checkpoint $module_dcp

      # Create the ports in the design
      dict for {port dir_width} $virtual_ports {
        lassign $dir_width dir width
        if {$width > 0} { create_port $port -direction $dir -from [expr $width -1] -to 0 }
      }

      set dcp "${modules_output}/${module}"
      write_checkpoint -force $dcp
      close_project
    }
  }
  return $add_virtual_ports
}


########################################################################################
# add virtual ports and lock pin constaints
#
# Arguments:
#
# Result:
#
########################################################################################
proc partition_nets::add_virtual_ports_and_lock_pin_constraints {modules inst_module modules_input modules_output virtual_ports} {
  # Check if need to add the virtual ports to the module
  set add_virtual_ports 1
  # foreach module $modules {
  #   set dcp "${modules_output}/${module}.dcp"
  #   if {![file exist $dcp]} { set add_virtual_ports 1; break; }
  # }

  if {$add_virtual_ports} {
    # Close any open project
    if {[current_project -quiet] != ""} {close_project}

    # Add the virtual ports to each module
    foreach module $modules {
      set module_dcp "${modules_input}/${module}.dcp"
      if {![file exist $module_dcp]} { logger.warning "Warning: Input DCP file does not exist: $module_dcp."; continue; }

      # Open the dcp file
      open_checkpoint $module_dcp

      # Create the ports in the design
      dict for {port dir_width} $virtual_ports {
        lassign $dir_width dir width
        if {$width > 0} { create_port $port -direction $dir -from [expr $width -1] -to 0 }
      }

      # Get all pin assignments of the LUT cells
      set lut_pin_assignments [dict create]
      set pins [get_pins -of_object [get_cells -hierarchical -filter { PRIMITIVE_TYPE =~ LUT.*.* }]]
      foreach pin $pins {
        set bel_pin  [get_bel_pins -of_objects $pin]
        dict set lut_pin_assignments $pin $bel_pin
      }

      # Save the pins assignment as the LOCK_PINS string property
      set cells_lockpins [dict create]
      dict for {cell_pin site_lutpin} $lut_pin_assignments {
        set cell [string range $cell_pin 0 [expr {[string last "/" $cell_pin] -1}]]
        set pin [lindex [split $cell_pin "/"] end]
        set lutpin [lindex [split $site_lutpin "/"] end]
        if {[regexp {[I][0-6]} $pin]} {
          set lockpin "$pin:$lutpin"
          dict_append_values cells_lockpins $cell $lockpin
        }
      }

      # Assign the complete LOCK_PINS constraint
      dict for {lut_cell lockpins} $cells_lockpins {
        # We only care for cells inside the module
        if {[string first $inst_module $lut_cell] != -1} {
          #logger.debug "lut_cell: $lut_cell, lockpins: $lockpins"
          set_property LOCK_PINS $lockpins [get_cells $lut_cell]
        }
      }

      # Set IS_ROUTE_FIXED
      # Does this resolve the overlapping node issue from ex1?
      set_property IS_ROUTE_FIXED 1 [get_nets -filter TYPE==SIGNAL]

      # Remove the DONT_TOUCH property on cells of the module
      set cells [get_cells -filter "DONT_TOUCH==1 && IS_PRIMITIVE==0"]
      foreach cell $cells {
        logger.debug "set_property DONT_TOUCH 0 for cell $cell"
        set_property DONT_TOUCH 0 [get_cells $cell]
      }

      set dcp "${modules_output}/${module}"
      write_checkpoint -force $dcp
      close_project
    }
  }
  return $add_virtual_ports
}


########################################################################################
# save_module_lock_pins
#
# Arguments:
#
# Result:
#
########################################################################################
proc partition_nets::save_module_lock_pins {modules modules_input modules_output} {

    # Close any open project
    if {[current_project -quiet] != ""} { close_project }

    # Add the virtual ports to each module
    foreach module $modules {
      set module_dcp "${modules_input}/${module}.dcp"
      if {![file exist $module_dcp]} { logger.warning "Warning: Input DCP file does not exist: $module_dcp."; continue; }

      # Open the dcp file
      open_checkpoint $module_dcp

      # Get all pin assignments if the LUTs in the project
      set lut_pin_assignments [dict create]
      set pins [get_pins -of_object [get_cells -hierarchical -filter { PRIMITIVE_TYPE =~ LUT.*.* }]]
      foreach pin $pins {
        set bel_pin  [get_bel_pins -of_objects $pin]
        dict set lut_pin_assignments $pin $bel_pin
      }

      # Save the input to physical pins assignment as one string.
      # This is the LOCK_PINS property.
      set cells_lockpins [dict create]
      dict for {cell_pin site_lutpin} $lut_pin_assignments {
        set cell [string range $cell_pin 0 [expr {[string last "/" $cell_pin] -1}]]
        set pin [lindex [split $cell_pin "/"] end]
        set lutpin [lindex [split $site_lutpin "/"] end]
        if {[regexp {[I][0-6]} $pin]} {
          set lockpin "$pin:$lutpin"
          dict_append_values cells_lockpins $cell $lockpin
        }
      }

      # Create a variable name for saving the cell lockpins
      set save_file "${module}_cells_lockpins"
      set $save_file $cells_lockpins

      slvar::save $save_file

      close_project
    }
}

# These are some predicate functions and should return 0 or 1.
proc f_net_exist_and_connected_to_cell { net cell } {
  if {$net == ""} {return 0}
  return [expr {[llength [get_nets -quiet -of_objects [get_cells -quiet $cell] -filter "NAME=~$net" ]] >0}]
}
proc f_has_mapping { k v } { return [expr {$v != ""}]}
proc f_contains {haystack needle} {return [expr {[string first $needle $haystack] != -1}]}
proc f_net_exist { n } { return [expr {[llength [get_nets $n -quiet]] >0}]}

proc compare_by_index {a b} {
  # regexp extracts the index: test[1] => 1
  set match_a [regexp {([\[]([0-9]+)[\]]$)} $a fullmatch br index_a]
  set match_b [regexp {([\[]([0-9]+)[\]]$)} $b fullmatch br index_b]

  if {$match_a && $match_b} {
    if {$index_a < $index_b} { return -1}
    if {$index_a > $index_b} { return 1}
  }
  return 0
}

proc partition_nets::remove_non_existing_nets {dmap {ignore_prefix {}}} {
  upvar $dmap map
  set existing_nets [dict filter $map script {k v} { expr {[f_net_exist $k]==1}}]

  if {[llength $existing_nets] == 0} { logger.error "The key nets for mapping do not exist in the design."}

  set new_map [dict create]
  # Remove nets that do not exist in the design
  dict for {f tt} $existing_nets {
    set nets $tt
    foreach t $tt {
      if {[f_net_exist $t] == 0} {
        logger.warning "Removed net '$t' from mapping, because it was not found (any more) in the design."
        set nets [ldelete $nets $t]
      }
    }
    dict append new_map $f $nets
  }
  set map $new_map
}


########################################################################################
# reconnect s2p and p2s nets
#
# Arguments:
#
# Result:
#
########################################################################################
proc partition_nets::reconnect_s2p_p2s_nets {upvar_mapping modules inst_partialarea inst_connectionprimitive inst_module} {
  # upvar $upvar_mapping mapping
  # can this work with the upvar_mapping?
  reconnect_s2p_static_nets upvar_mapping $modules $inst_partialarea $inst_connectionprimitive $inst_module
  reconnect_p2s_static_nets upvar_mapping $modules $inst_partialarea $inst_connectionprimitive $inst_module
}


########################################################################################
# reconnect p2s static nets
#
# Arguments:
#
# Result:
#
########################################################################################
proc partition_nets::reconnect_p2s_static_nets {upvar_mapping modules inst_partialarea inst_connectionprimitive inst_module} {
  upvar $upvar_mapping mapping
  variable p2s_static_net_combined

  remove_non_existing_nets mapping
  set valid_nets [dict filter $mapping script {k v} { expr [f_has_mapping $k $v] && [f_contains $k $inst_connectionprimitive]}]
  set debug 1

  if {[llength $valid_nets] < 1 } {
    set where [dict get [info frame 0] proc]
    logger.error "No valid nets for mapping. $where"
  } else {
    set from_nets [dict keys $valid_nets]
    set to_nets [dict values $valid_nets]
    set replace_by [list ${inst_partialarea}/ ""]
    set to_nets [lmap i $to_nets { string map $replace_by $i}]
    set all_from_nets [get_nets -quiet $from_nets]
    set all_to_nets [get_nets -quiet $to_nets]
    set m2s_map [dict create]
    set m2m_map [dict create]

    if {[llength $all_from_nets] < 1} {logger.error "No nets to start with. The 'from' nets is empty."}
    if {[llength $all_to_nets] < 1} {logger.error "No nets to start with. The 'to' nets is empty."}

    # Create m2s (module to static) and m2m (module 2 module) mapping dict
    set j 0
    foreach f $all_from_nets t $all_to_nets {
      if {$f != ""} {
        set m [get_nets [get_nets $f -segments] -filter "PARENT_CELL=~*${inst_module}*"]
        set tm [get_nets -quiet [get_nets -quiet -segments $t] -filter "PARENT_CELL=~*${inst_module}"]

        if {[llength $tm] > 1} {logger.error "Expected a single net to a module but '$tm' found.\nFrom net '$f' to net '$t'."}

        if {[llength $m] != 1} {
          # Pick the net with the shortest hierarchy
          set i 0
          set shortest -1
          set idx 0
          foreach mm $m {
            set depth [expr {[llength [split $mm "/"]] - 1}]

            if {($depth < $shortest || $shortest == -1)} {
              set shortest $depth
              set idx $i
            }

            incr i 1
          }
          set m [lindex $m $idx]
        }

        if {$tm != ""} {
          dict_append_values m2s_map $m $tm
          dict_append_values m2m_map $m $tm
        } else {
          dict_append_values m2s_map $m $t
        }
      }
      incr j 1
    }

    # Get the pass through nets from the mapping dict.
    # Those nets are not connected to any input or output inside the module
    # They appear both as a key and as value.
    set m2m_ptn [dict create]
    set values [dict values $m2m_map]
    dict for {from_net to_nets} $m2m_map {
      foreach value $values {
        if {$from_net == $value} {dict set m2m_ptn $from_net ""}
      }
    }

    # Get additional to_nets for the pass through nets
    dict for {from_net to_nets} $m2s_map {
      if {[dict exists $m2m_ptn $from_net]} {
        foreach to_net $to_nets { dict_append_values m2m_ptn $from_net $to_net}
      }
    }

    set m2m_passthrough_map [dict create]

    if {[dict size $m2m_ptn] > 0} {
      if {$debug} {
        logger.debug "m2m_map size before [dict size $m2m_map]"
        logger.debug "m2m ptn: $m2m_ptn"
      }

      # Remove pass through route from mapping
      dict for {from_net to_net} $m2m_ptn {set m2m_map [dict remove $m2m_map $from_net]}

      # Replace pass through nets with the actual end nets in m2s
      set _m2s_map $m2s_map
      dict for {from_net to_nets} $_m2s_map {
        set replace 0
        set i 0
        foreach to_net $to_nets {

          # Is this net a pass through net?
          if {[dict_try_get_value m2m_ptn $to_net end_nets]} {
            dict append m2m_passthrough_map $from_net $to_net

            # Replace by actual end nets
            set nets [lreplace nets $i $i {*}$end_nets]
            set replace 1
            break;
          }
          incr i 1
        }

        if {$replace} {
          dict set m2s_map $from_net $nets
          #logger.debug "replaced in m2s_map: $from_net, nets: $nets"
        }
      }

      # Remove the pass through nets from mapping
      set count 0
      dict for {from_net to_net} $m2m_ptn {
        if {[dict exists $m2s_map $from_net]} {
          set m2s_map [dict remove $m2s_map $from_net]
          incr count 1
        }
      }
      if {$count > 0} {logger.info "Removed $count pass through net(s) from m2s_map, last one is $from_net"}

      if {$debug} {
        logger.debug "m2m_map size after: [dict size $m2m_map]"
        logger.debug "m2m_passthrough_map (1st): [dict_first_pair m2m_passthrough_map]"
        logger.debug "m2s_map (1st): [dict_first_pair m2s_map]"
      }

      # Assign route to begin nets of ptn
      set from_nets [dict keys $m2m_passthrough_map]
      set to_nets [dict values $m2m_passthrough_map]
      set from_nets_route [get_abs_route [get_nets $from_nets]]
      set to_nets_route [get_abs_route [get_nets $to_nets]]

      # Will matching on the first node work better here?
      set first_node 1; # Return the first node found in the route string
      set net1_remove_till_end 1;
      set nodes [get_net_intersections $from_nets_route $to_nets_route $m2m_passthrough_map $first_node]
      set combined [combine_nets $from_nets_route $to_nets_route $nodes $m2m_passthrough_map $net1_remove_till_end]

      # Can we apply this route?
      logger.debug "Apply pass through route..."
      set i 1
      dict for {net route} $combined {
        logger.debug "$i/[dict size $combined] net: $net, route \n$route"
        set_property IS_ROUTE_FIXED 0 [get_nets $net]
        set routestring [get_route_string_from_route_list $route]
        if {[is_balanced $route] != 1} {logger.error "Braces in route of net $net are not balanced."}
        set_property ROUTE {} [get_nets $net]
        set_property ROUTE $routestring [get_nets $net]
        set_property IS_ROUTE_FIXED 1 [get_nets $net]
        incr i 1
      }

      logger.debug "Apply pass through route...OK."
    }

    # Get all values as separate list items from the dict.
    set static_nets [list]
    dict for {key values} $m2s_map { foreach value $values { lappend static_nets $value }}
    set static_nets [lsort -unique $static_nets]
    set module_nets [lsort -unique [dict keys $m2s_map]]
    if {[llength $static_nets] < 1} {logger.error "There are no static nets."}
    if {[llength $module_nets] < 1} {logger.error "There are no module nets."}

    # Get the absolute route string for each net
    set static_nets_route [get_abs_route [get_nets $static_nets]]
    set module_nets_route [get_abs_route [get_nets $module_nets]]

    # The module to module nets
    set m2m_modules_nets [dict values $m2m_map]

    # Get the nodes of intersection
    partition_nets::trace "reconnect_p2s_static_nets intersecting nodes m2s_map"
    set nodes [get_net_intersections $module_nets_route $static_nets_route $m2s_map]

    # Combine the nets
    partition_nets::trace "1. reconnect_p2s_static_nets combine_nets"
    set p2s_static_net_combined [combine_nets $module_nets_route $static_nets_route $nodes $m2s_map]

    # Remove any antenna nets
    set all_nets_endroutenodes [partition_nets::get_nets_endroutenodes $p2s_static_net_combined]
    set used_nets_endroutenodes [partition_nets::get_nets_endroutenodes $static_nets_route]
    set nets_antenna_nodes [partition_nets::get_nets_antenna_nodes $all_nets_endroutenodes $used_nets_endroutenodes $m2s_map]
    partition_nets::trace "2. reconnect_p2s_static_nets remove_antennas"
    partition_nets::remove_antennas p2s_static_net_combined $nets_antenna_nodes
    set nets_antenna_nodes [dict filter $nets_antenna_nodes script {k v} { expr {[f_has_mapping $k $v]==1}}]

    # Remove all p2s connection primitive nets
    set cells [list]
    foreach module $modules {
      lappend cells {*}[get_cells -quiet $module/$inst_connectionprimitive*]
    }
    delete_connectionprimitives $cells

    # Delete any intermediate signals
    partition_nets::trace "3. reconnect_p2s_static_nets delete_intermediate_signals"
    delete_intermediate_signals $module_nets
    if {[dict size $m2m_map] > 0} {delete_intermediate_signals $m2m_modules_nets}

    partition_nets::trace "4. reconnect_p2s_static_nets remove constraints"
    remove_constraints $static_nets $inst_partialarea

    # Temporary remove the DONT_TOUCH property of all module nets
    set nets_dont_touch [list]
    foreach module $modules {
      lappend nets_dont_touch {*}[get_nets [get_nets $module/* -segments] -filter DONT_TOUCH==1]
    }

    if {[llength $nets_dont_touch] > 0} {
      logger.debug "Set property DONT_TOUCH 0 of [llength $nets_dont_touch] net(s)..."
      set_property DONT_TOUCH 0 [get_nets $nets_dont_touch]
    }

    # Reconnect the nets
    partition_nets::trace "5. reconnect_p2s_static_nets reconnect_nets"
    partition_nets::reconnect_nets $m2s_map $inst_partialarea 0 1

    # Restore the lock pins
    partition_nets::trace "7. reconnect_p2s_static_nets restore_lock_pins..."
    partition_nets::restore_lock_pins $p2s_static_net_combined
    partition_nets::trace "7. reconnect_p2s_static_nets restore_lock_pins...ok"

    # Set the new route
    logger.debug "size: [dict size $p2s_static_net_combined]"
    partition_nets::trace "8. reconnect_p2s_static_nets set_property_route_nets"
    partition_nets::set_property_route_nets $p2s_static_net_combined 1

    # Restore the DONT_TOUCH property
    if {[llength $nets_dont_touch] > 0} {
      logger.debug "Set property DONT_TOUCH 0 of [llength $nets_dont_touch] net(s)..."
      set_property DONT_TOUCH 1 [get_nets $nets_dont_touch]
    }
  }
}


proc partition_nets::restore_lock_pins {dnets} {
  set nets [dict keys $dnets]

  if {![slvar::load lut_pin_assignments]} {
     logger.warning "Warning: variable 'lut_pin_assignments' could not be loaded. Proc 'restore_lock_pins' can not be executed."
     return
  }

  # Get the lockpins as a complete string
  set cells_lockpins [dict create]
  dict for {cell_pin site_lutpin} $lut_pin_assignments {
    set cell [lindex [split $cell_pin "/"] 0]
    set pin [lindex [split $cell_pin "/"] end]
    set lutpin [lindex [split $site_lutpin "/"] end]
    if {[regexp {[I][0-6]} $pin]} {
      set lockpin "$pin:$lutpin"
      dict_append_values cells_lockpins $cell $lockpin
    }
  }

  # Get all the cells in the design for the specified nets
  set cells [list]
  foreach net $nets {
    set in_pins [get_pins -leaf -of_objects [get_nets $net] -filter "DIRECTION==IN"]
    foreach in_pin $in_pins {
      set act_bel_pin [get_bel_pins -of_objects [get_pins $in_pin]]
      if {[dict_try_get_value lut_pin_assignments $in_pin org_bel_pin]} {
        set cell [get_cells -of_objects [get_pins $in_pin]]
        lappend cells $cell
      }
    }
  }
  set cells [lsort -unique $cells]

  # Restore the lockpins for each cell
  foreach cell $cells {
    if {[dict_try_get_value cells_lockpins $cell lockpins]} {
      set_property LOCK_PINS "$lockpins" [get_cells $cell]
    }
  }

}

proc partition_nets::get_nets_endroutenodes {dnets} {
  set nodes [dict create]
  dict for {net route_nodes} $dnets {
    set i 0
    dict set nodes $net {}
    foreach node $route_nodes {
      if {[lindex $route_nodes $i+1] == "\}"} { dict_append_values nodes $net $node }
      incr i 1
    }
  }
  return $nodes
}

proc partition_nets::get_nets_antenna_nodes {dall_nets_endnodes dused_nets_endnodes dmap} {
  set nets_antenna_nodes [dict create]
  dict for {net nodes } $dall_nets_endnodes {
    if {[dict_try_get_value dmap $net s_nets]} {
      set antenna_nodes $nodes
      foreach s_net $s_nets {
        if {[dict_try_get_value dused_nets_endnodes $s_net s_nodes]} {
          foreach s_node $s_nodes {
            set i [lsearch $antenna_nodes $s_node]

            # If the node is in the list of used nodes, it is not an antenna node. Remove the node from the list.
            if {$i != -1} { set antenna_nodes [lreplace $antenna_nodes $i $i]}
          }
        }
      }
      set antenna_nodes [lsort -unique $antenna_nodes]
      dict_append_values nets_antenna_nodes $net $antenna_nodes
    }
  }
  return $nets_antenna_nodes
}

proc partition_nets::remove_antennas {upvar_dnets dnets_antennas} {
  upvar $upvar_dnets nets
  # create a local copy
  set _nets $nets

  dict for {net nodes} $_nets {
    if {[dict_try_get_value dnets_antennas $net antenna_nodes]} {
      if {[llength $antenna_nodes] > 0} {
        set new_nodes $nodes
        foreach antenna_node $antenna_nodes {
          # Find the begin, this either a open or close brace. Find the end, a close brace.
          set i [lsearch $new_nodes $antenna_node]
          if {[lindex $new_nodes $i+1] == "\}" } {
            set braces [lsearch -regexp -all $new_nodes {^[\{\}]$}]
            set begin 0

            # Get the first brace before the node
            foreach brace $braces {
              if {$brace > $i} { break }
              set begin $brace
            }

            if {$begin > 0 && $begin < $i} {
              # Check if the node is between a open and close brace, then we remove those as well.
              if {[lindex $new_nodes $begin]=="\{"} {
                set new_nodes [lreplace $new_nodes $begin $i+1]
              } else {
                # else only remove the nodes.
                set new_nodes [lreplace $new_nodes $begin+1 $i]
              }
            }

          } else {
            set node [lindex $new_nodes $i]
            logger.debug "new_nodes: $new_nodes, i: $i"
            logger.warning "Warning: Node '$node' of net '$net' has no proceeding close bracket and is skipped."
          }
        }

        # Remove any trailing wires (those wires are not pins)
        # check if the node list ends with a pin wire
        partition_nets::remove_trailing_wires new_nodes

        # Check if the node list ends with a branch, this is not allowed by Vivado.
        # to fix this, we can remove the brackets from this branch.
        partition_nets::remove_braces_of_last_branch new_nodes
        dict set nets $net $new_nodes
      }
    }
  }
}


proc partition_nets::remove_trailing_wires {upvar_lnodes} {
  upvar $upvar_lnodes lnodes
  set _nodes $lnodes
  for {set index [expr [llength $_nodes] -2]} { $index > 0 } { incr index -1 } {
    set node [lindex $_nodes $index]
    if {$node == "\}"} {break}
    if {[is_absolute_wire $node]==0} {continue}
    if {[get_property IS_PIN [get_nodes $node]]} {break}
    set i [lsearch $lnodes $node]
    if {$i != -1} {set lnodes [lreplace $lnodes $i $i]}
  }
}


proc partition_nets::remove_braces_of_last_branch {upvar_lnodes} {
  upvar $upvar_lnodes lnodes
  set second_last_node [lindex $lnodes end-1]

  # The second last node is a closing brace.
  if {$second_last_node == "\}"} {
    set i_ob -1
    set cb_count 0
    set start [expr [llength $lnodes] -2]

    # Start at the end of the list to find the matching open brace.
    for {set i $start} {$i > -1} { incr i -1} {
      set brace [lindex $lnodes $i]
      if {$brace == "\}"} {
        incr cb_count 1
      } elseif {$brace == "\{"} {
        incr cb_count -1
        if {$cb_count == 0} {
          # The index for the matching open brace
          set i_ob $i
          break
        }
      }
    }
    if {$i_ob != -1} {
      set lnodes [lreplace $lnodes $i_ob $i_ob]
      set lnodes [lreplace $lnodes end end]
    }
  }
}


# reconnect s2p static nets
proc partition_nets::reconnect_s2p_static_nets {upvar_mapping modules inst_partialarea inst_connectionprimitive inst_module} {
  upvar $upvar_mapping mapping
  remove_non_existing_nets mapping
  set valid_nets [dict filter $mapping script {k v} { expr [f_has_mapping $k $v] && [f_net_exist_and_connected_to_cell $k $inst_partialarea]} ]

  if {[llength $valid_nets] < 1 } {
    set where [dict get [info frame 0] proc]
    logger.warning "No valid nets for mapping. $where"
  } else {

    set from_nets [dict keys $valid_nets]
    set to_nets [dict values $valid_nets]
    set replace_by [list ${inst_partialarea}/ ""]
    set from_nets [lmap i $from_nets { string map $replace_by $i}]
    set static_nets [get_nets -quiet $from_nets]
    set connection_nets [get_nets -quiet $to_nets]

    set s2m_map [dict create]
    foreach s_net $static_nets c_net $connection_nets {
      if {$c_net != ""} {
        set m_nets [get_nets [get_nets $c_net -segments] -filter "PARENT_CELL=~*${inst_module}*"]

        # if more than one net, take the first one
        lassign $m_nets m_net
        dict append s2m_map $s_net $m_net
      }
    }

    set module_nets [lsort -unique [dict values $s2m_map]]
    if {[llength $static_nets] < 1} {logger.error "No static nets to reconnect"}
    if {[llength $module_nets] < 1} {logger.error "No module nets to reconnect"}

    # Get the absolute route string for each net
    set static_nets_route [get_abs_route [get_nets $static_nets]]
    set module_nets_route [get_abs_route [get_nets $module_nets]]

    # Get the nodes of intersection
    set nodes [get_net_intersections $static_nets_route $module_nets_route $s2m_map]

    # Combine the nets
    set s2p_static_net_combined [combine_nets $static_nets_route $module_nets_route $nodes $s2m_map]

    # Remove all s2p connection primitive nets
    set cells [get_cells_connectionprimitive $module_nets $inst_connectionprimitive]
    delete_connectionprimitives $cells
    delete_intermediate_signals $module_nets
    remove_constraints $static_nets $inst_partialarea
    reconnect_nets $s2m_map $inst_partialarea 1
    delete_pins_from_partialarea [dict keys $s2m_map] $inst_partialarea
    set_property_route_nets $s2p_static_net_combined 0
  }
}

proc partition_nets::get_cells_connectionprimitive {lnets conn_prim_name} {
  if {[llength $lnets]>0} {
    set cells [lsort -unique [get_property PARENT_CELL [get_nets [get_nets $lnets -segments] -filter "NAME=~*${conn_prim_name}*"]]]
    return $cells
  }
  return [list]
}

proc partition_nets::delete_connectionprimitives {lcells} {
  if {[llength $lcells]>0} {
    set pblocks [get_pblocks -of_objects [get_cells $lcells]]
    remove_cell $lcells
    logger.debug "delete_connectionprimitives: $lcells"
    delete_pblocks [get_pblocks $pblocks]
  }
}

proc partition_nets::get_bus_nets {lnets} {
  set buses [get_property BUS_NAME [get_nets $lnets]]
  set buses [lsort -unique $buses];              # remove duplicates
  set buses [struct::list flatten -full $buses]; # remove empty elements
  return $buses
}

proc partition_nets::get_bus_nets2 {lnets} {
  set buses [dict create]
  foreach net $lnets {
    # filter each net by <cell/net name><[index]>
    set match [regexp {(^[A-Za-z0-9\/_]+)([\[][0-9]+[\]])} $net fullmatch name idx]
    if {$match} {
      dict append buses $name ""
    }
  }
  return [dict keys $buses]
}

proc partition_nets::delete_intermediate_signals {lnets} {
  # Delete any intermediate signal nets
  # These nets are the signals in the VHDL source.
  if {[llength $lnets]>0} {
    set nets [get_nets $lnets -top_net_of_hierarchical_group -segments]
    set buses [get_bus_nets2 $nets]
    logger.warning "delete_intermediate_signals: going to remove nets: $buses"
    foreach bus $buses {remove_net $bus}
  }
}

proc partition_nets::remove_constraints {lnets inst_partialarea} {
  set_property HD.PARTITION 0 [get_cells $inst_partialarea]
  set_property DONT_TOUCH 0 [get_cells $inst_partialarea]
  set_property DONT_TOUCH 0 [get_nets $lnets]
}

proc partition_nets::reconnect_nets {dmapped_nets inst_partialarea {s2p 1} {debug 0}} {
  set i 1
  set trigger 0

  # Connect all mapped nets
  dict for {from_net to_nets} $dmapped_nets {
    if {$debug} {logger.debug "$i/[dict size $dmapped_nets] from net: $from_net to nets $to_nets"}

    lassign $to_nets to_net

    if {$s2p == 1} {
      if {[llength $to_nets] > 1} { logger.error "Not implemented: s2p mapping to multiple nets." }
      connect_net -net [get_nets $from_net] -objects [get_pins $to_net] -hierarchical
    } else {

      set connect_pins [list]
      foreach net $to_nets {
        set pins [get_pins -of_objects [get_nets $net -segments] -quiet -filter "DIRECTION==IN"]
        set out_pins [get_pins -of_objects [get_nets $net -segments] -quiet -filter "DIRECTION==OUT"]
        if {($debug || $trigger)} {logger.debug "net $net, pins $pins"}
        if {[llength $pins] > 0} {
          lappend connect_pins {*}$pins
        }
      }

      if {($debug || $trigger)} {logger.debug "connect_pins $connect_pins"}

      if {$debug} {logger.debug "disconnect_net..."}
      disconnect_net -objects [get_pins $connect_pins]

      if {$debug} {logger.debug "connect_net..."}
      connect_net -net [get_nets $from_net] -objects [get_pins $connect_pins] -hierarchical
    }
    incr i 1
  }
}

proc partition_nets::delete_pins_from_partialarea {lnets partialarea} {
  logger.todo "Disabled delete_pins_from_partialarea 210426, example1 fails on it."
  #set buses [get_bus_nets $lnets]
  #logger.log "delete_pins_from_partialarea partialarea $partialarea, buses $buses"
  #foreach bus $buses { remove_pin "${partialarea}/$bus" }
}

proc partition_nets::set_property_route_nets {dnets {debug 0}} {
  set nets [dict keys $dnets]
  set_property IS_ROUTE_FIXED 0 [get_nets $nets]
  set i 1
  #set debug 1
  set halt_on_error 0
  set trigger 0
  set trigger_value "inst_AES/AESRound/x0y1_s2p_w[78]"

  # Write the new ROUTE string back to the static net
  dict for {net route} $dnets {
    if {$debug} {logger.debug "($i/[dict size $dnets]) set_property_route_nets net: $net"}
    if {$net == $trigger_value} {set trigger 1} else {set trigger 0}

    set routestring [get_route_string_from_route_list $route]

    #if {$debug} {logger.debug "clear route..."}
    set_property ROUTE {} [get_nets $net]
    #if {$debug} {logger.debug "clear route...ok"}

    # Note: we are unable the catch some errors here, which looks like a bug.
    # e.g the error: ERROR: [Designutils 20-941] Did not find node resource, IMUX_L4, downhill from node, INT_L_X30Y70/EE2BEG1.
    # For now we use the -quiet option to continue the program in case of an error.
    # Later on we check the route status of each net.
    if {$halt_on_error} {
      if {$debug} {logger.debug "set ROUTE $routestring"}
      set_property ROUTE $routestring [get_nets $net]
    } else {
      if {$debug} {logger.debug "set ROUTE $routestring"}
      if {[catch {set_property ROUTE $routestring [get_nets $net]} res opt]} {
        logger.error "Could not apply route to net '$net': $res $opt\nRoute string $routestring"
      }
    }

    incr i 1
  }

  set_property IS_ROUTE_FIXED 1 [get_nets $nets]

  # Log the route status when not ok
  set nets_status [get_property ROUTE_STATUS [get_nets $nets]]
  foreach net $nets {
    set status [get_property ROUTE_STATUS [get_nets $net]]
    if {!($status == "ROUTED" || $status == "INTRASITE")} {
      logger.warning "Net '$net' has route status '$status'."
    }
  }
}


proc partition_nets::restore_design_constraints {inst_partialarea} {
  if {[slvar::load lut_pin_assignments]==1} {
    set_lut_pin_assignments $lut_pin_assignments $inst_partialarea
    logger.debug "Restore all lockpins LUTs...done"
  } else {logger.warning "Could not restore lock pins of LUTs, the export file does not exist."}
}


proc partition_nets::restore_clock_nets {modules inst_module internal_module_clk external_module_clk} {
  # TODO proper way to detect clock networks
  # This method below detects and merges clock nets and logic.
  # Note: for GoAhead specific clock configuration.
  # clk: the net and clock signal for the module
  # fclk_clk0 the clock logic outside the module

  if {$internal_module_clk != "" && $external_module_clk != ""} {
    foreach module $modules {
      set clk_net $module/$inst_module/$internal_module_clk
      set clk_pins [get_pins -of_objects [get_nets $clk_net] -filter "IS_CLOCK==1 && NAME=~*/C"]

      # Remove the external clock nets
      remove_net $module/${external_module_clk}_IBUF_BUFG
      remove_net $module/${external_module_clk}_IBUF
      remove_net $module/${external_module_clk}

      # Remove the external clock cells
      remove_cell $module/${external_module_clk}_IBUF_inst
      remove_cell $module/${external_module_clk}_IBUF_BUFG_inst

      # Disconnect the clock pins
      set module_clock_pins [get_pins -of_objects [get_nets $clk_net] -filter "IS_CLOCK==1 && NAME=~*/C"]
      disconnect_net -pinlist [get_pins $module_clock_pins]

      # Note: Goahead specific placed flipflops
      set unplaced_cells [get_cells -filter "STATUS==UNPLACED && NAME=~SLICE_* && REF_NAME==FDRE"]
      remove_cell $unplaced_cells

      # Connect the clock net to the global clock net
      logger.debug "module clock pins: $module_clock_pins"
      connect_net -net ${internal_module_clk}_IBUF_BUFG -object [get_pins $module_clock_pins] -hierarchical
      route_design -nets [get_nets ${internal_module_clk}_IBUF_BUFG]
    }
  }
}


proc partition_nets::remove_unused_interface_module {modules_config interface_map inst_module modules_output} {
  # Search for the REMOVE_INTERFACE keyword in the interface_map and delete this interface from the module before placement
  #{module_SubBytes_config1/inst_ConnectionPrimitiveNorthOutput/x0y0_p2s_n[*]} {REMOVE_INTERFACE}
  #set pattern {([A-Z_A-Z]*[\s])([x])([0-9]+)([y])([0-9]+)(:)([x])([0-9]+)([y])([0-9]+)}

  dict for {key value} $interface_map {
    # use regular expression here to get the cooordinates
    set rem_intf ""
    set match [regexp {([A-Z_A-Z]*[\s])([x])([0-9]+)([y])([0-9]+)(:)([x])([0-9]+)([y])([0-9]+)} $value fullmatch rem_intf x1 topleft_x y1 topleft_y colon x2 bottomright_x y2 bottomright_y]

    if {$match==1 && $rem_intf == "REMOVE_INTERFACE"} {
      # The key should start with the name of the module
      set module [lindex [split $key /] 0]

      logger.debug "module: $module"

      # Check if the module exist in the configuration
      foreach module_config $modules_config {
        if {$module == $module_config} {

          # Open the DCP file of the corresponding module
          set module_dcp "${modules_output}/${module_config}.dcp"
          if {![file exist $module_dcp]} {
            logger.warning "Warning: DCP file does not exist: $module_dcp."; continue;
          } else {

            # Close any open project
            if {[current_project -quiet] != ""} {close_project}

            # Open the dcp file
            open_checkpoint $module_dcp
            set node ""

            # Remove the module name from the key (we have an unplaced module here)
            set net [string replace $key 0 [string length $module_config] ""]
            set nets_route [get_abs_route [get_nets $net]]
            logger.debug "net: $net xy: $topleft_x $topleft_y $bottomright_x $bottomright_y, nets: [dict size $nets_route]"

            # Check if there are nets to modify
            if {[dict size $nets_route]>0} {
              # not used?
              set SB_North_nets_node [dict create]

              # Create a copy of the dict to keep itteration and modification separate.
              set _nets_route $nets_route

              set i 1
              set size [dict size $_nets_route]
              dict for {net route} $_nets_route {
                set node ""
                set idx [get_first_node_in_region $route $topleft_x $topleft_y $bottomright_x $bottomright_y node]
                dict set SB_North_nets_node $net $node

                if {$idx != -1} {
                  set new_route [remove_branch $route $node]
                  dict set nets_route $net $new_route
                }
                incr i 1
              }

              set i 1
              set size [dict size $nets_route]
              dict for {net route} $nets_route {
                #logger.debug "($i/$size) Net: $net"
                set routestring [get_route_string_from_route_list $route]
                set actualroute [get_property ROUTE [get_nets $net]]
                set_property ROUTE {} [get_nets $net]
                set_property ROUTE $routestring [get_nets $net]
                incr i 0
              }
              write_checkpoint $module_dcp -force
            }
            close_design
          }
        }
      }
    }
  }
}
