
##########################################################################################
# A simple logger utility
##########################################################################################
namespace eval logger {

  # exports
  namespace export log_message

  # variables
  variable logger_level 1
  variable reporting_level 1
  variable log_dir ""
  variable log_file "logger.log"

  proc create {logdir {logfile {logger.log}}} {
    variable log_dir
    variable log_file
    set log_dir $logdir
    set log_file $logfile
    exec mkdir -p $log_dir
    file delete $log_dir/$log_file
  }

  proc log_message {type message} {
    variable reporting_level
    if {$reporting_level > 0} {
      variable logger_level
      variable log_dir
      variable log_file

      set datetime [clock format [clock seconds] -format %H:%M:%S]
      set log_msg [format "%s %+7s: %s" $datetime $type $message]
      puts $log_msg

      if {$logger_level > 0 & $log_dir != "" & $log_file != ""} {
        # Open file for appending
        set logfile $log_dir/$log_file
        set fp [open $logfile a]
        puts $fp $log_msg
        close $fp
      }
    }
  }

  proc logger_error {message} {
    log_message "error" $message
    error "error: $message"
  }

  proc logger_warning {message} {
    log_message "warning" $message
    warning_vivado $message
  }

  # Write a variable and its value to a text file
  proc logger_var {varname varvalue} {
    # Open file for appending
    set fp [open $log_dir/logger_var.log a]
    puts $fp "set ${varname} {${varvalue}}"
    puts $fp ""
    close $fp
  }


  proc logger_log {message} { log_message   "log" $message }
  proc logger_info {message} { log_message  "info" $message }
  proc logger_trace {message} { log_message "trace" $message }
  proc logger_debug {message} { log_message "debug" $message }
  proc logger_todo {message} { log_message  "todo" $message }

  # Print a warning in the Vivado TCL console (orange text)
  proc warning_vivado {message {where {}}} {
    if {[llength [info commands "send_msg_id"]] > 0} {
      set msg "$message $where"
      send_msg_id "USER 1-1" {WARNING} "$msg"
    }
  }

}

# Define some helper procs
proc logger.error msg {logger::logger_error $msg}
proc logger.log msg {logger::logger_log $msg}
proc logger.info msg {logger::logger_info $msg}
proc logger.trace msg {logger::logger_trace $msg}
proc logger.debug msg {logger::logger_debug $msg}
proc logger.todo msg {logger::logger_todo $msg}
proc logger.warning msg {logger::logger_warning $msg}

proc logger.var {varname varvalue} {logger::logger_var $varname $varvalue}
