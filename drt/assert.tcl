proc assert condition {
  if {![uplevel 1 expr $condition]} {
   return -code error "Assertion failed: $condition"
  }
}

proc assert_eq {a b} {
  if {$a ne $b} {
   return -code error "Assertion equality failed: '${a}' eq '${b}'"
  }
}