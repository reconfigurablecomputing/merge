##########################################################################################
# Design Reconstruction Tool (DRT)
#
# In the Vivado console:
#   cd /home/jeroen/git/Merge/drt
#   source drt.tcl
#   drt::merge <arguments>
##########################################################################################
package require Tcl 8.0
package require struct::set
package require struct::list

# source all the required files
cd [file dirname [file normalize [info script]]]
source list_extensions.tcl
source dict_extensions.tcl
source logger.tcl
source slvar.tcl
source utils_debug.tcl
source utils.tcl
source utils_vivado.tcl
source utils_net_route.tcl
source partition_nets.tcl
source anchor_logic.tcl

namespace eval drt {
  # exports
  namespace export create
  namespace export clean
  namespace export merge

  # variables (initialize and set defaults)
  variable path_to_drt               [file dirname [file normalize [info script]]]
  variable modules_input             "${path_to_drt}/input/modules"
  variable modules_output            "${path_to_drt}/output/modules"
  variable static_system_input       "${path_to_drt}/input/staticsystem"
  variable static_system_output      "${path_to_drt}/output/staticsystem"
  variable log_dir                   "${static_system_output}/.log"
  variable saved_dir                 "${static_system_output}/.saved"
  variable script_mapping            "${path_to_drt}/input/mapping.tcl"
  variable inst_partialarea          "inst_PartialArea"
  variable inst_module               "inst_Module"
  variable inst_connectionprimitive  "inst_ConnectionPrimitive"
  variable modules_config [list]
  variable interface_map [list]
  variable unused_s2p [list]
  variable unused_p2s [list]
  variable internal_module_clk ""
  variable external_module_clk ""
  variable static_system_name ""
  variable static_system_input_dcp ""
  variable modules_dcp [list]
}

########################################################################################
# Create all the objects
########################################################################################
proc drt::create {} {
  cd $drt::path_to_drt
  partition_nets::create 1
  logger::create $drt::log_dir
  slvar::create $drt::saved_dir
}

########################################################################################
# Clean up the workspace
########################################################################################
proc drt::clean {} {
  cd $drt::path_to_drt
  if {[current_project -quiet] != ""} {close_design}
  lappend files {*}[glob -nocomplain $drt::saved_dir/*_saved.tclvar]
  lappend files {*}[glob -nocomplain hs_err_*.log]
  lappend files {*}[glob -nocomplain $drt::modules_output/*.dcp]
  lappend files {*}[glob -nocomplain $drt::static_system_output/*.dcp]
  lappend files $drt::log_dir/logger.log
  if {[llength $files] > 0} {file delete {*}$files}
}

########################################################################################
# Merge the input design files
########################################################################################
proc drt::merge {inputdir_modules outputdir_modules inputdir_static outputdir_static mapping_config_file {name_inst_partialarea "inst_PartialArea"} {name_inst_module "inst_Module"} {name_inst_connectionprimitive "inst_ConnectionPrimitive"}} {
  variable modules_input
  variable modules_output
  variable static_system_input
  variable static_system_output
  variable script_mapping
  variable inst_partialarea
  variable inst_module
  variable inst_connectionprimitive
  variable log_dir
  variable saved_dir

  set modules_input $inputdir_modules
  set modules_output $outputdir_modules
  set static_system_input $inputdir_static
  set static_system_output $outputdir_static
  set script_mapping $mapping_config_file
  set inst_partialarea $name_inst_partialarea
  set inst_module $name_inst_module
  set inst_connectionprimitive $name_inst_connectionprimitive
  set log_dir "${static_system_output}/.log"
  set saved_dir  "${static_system_output}/.saved"

  drt::run_merge
}

proc drt::run_merge {} {
  # Make access to the namespace variables
  variable modules_input
  variable modules_output
  variable static_system_input
  variable static_system_output
  variable script_mapping
  variable inst_partialarea
  variable inst_module
  variable inst_connectionprimitive
  variable modules_config
  variable interface_map
  variable unused_s2p
  variable unused_p2s
  variable internal_module_clk
  variable external_module_clk
  variable static_system_input_dcp
  variable static_system_name
  variable modules_dcp

  drt::clean
  drt::create
  drt::start_script
  drt::vivado_settings
  drt::get_dcp_files $static_system_input $modules_input static_system_input_dcp static_system_name modules_dcp
  drt::read_mapping $script_mapping

  # Close any open design and open the checkpoint file
  if {[current_project -quiet] != ""} {close_design}
  open_checkpoint $static_system_input_dcp

  ##########################################################################################
  # Preserve anchor logic, before placing modules
  drt::preserve_anchor_logic

  ##########################################################################################
  # Prepare the modules for placing
  drt::prepare_modules_for_placing
  if {[current_project -quiet] == ""} {open_checkpoint $static_system_input_dcp}

  ##########################################################################################
  # Prepare the static design for module placement
  drt::prepare_staticdesign_for_placement

  ##########################################################################################
  # Place the module into the static design
  partition_nets::place_modules $modules_config [glob "${modules_output}/*.dcp"]

  # Check if we have any unplaced cells in the design after placing the modules
  drt::check_any_unplaced_cells

  ##########################################################################################
  # Clear the nets not used for this configuration
  drt::clear_nets_not_used

  ##########################################################################################
  # Reconnect all nets
  save_checkpoint "${static_system_output}/${static_system_name}_modules_placed.dcp"
  partition_nets::reconnect_s2p_static_nets interface_map $modules_config $inst_partialarea $inst_connectionprimitive $inst_module
  save_checkpoint "${static_system_output}/${static_system_name}_nets_reconnected_s2p.dcp"
  partition_nets::reconnect_p2s_static_nets interface_map $modules_config $inst_partialarea $inst_connectionprimitive $inst_module
  save_checkpoint "${static_system_output}/${static_system_name}_nets_reconnected_p2s.dcp"

  ##########################################################################################
  # Remove non connected nets
  drt::remove_cell_partial_area
  save_checkpoint "${static_system_output}/${static_system_name}_remove_cell_partial_area.dcp"

  ##########################################################################################
  # Handle unused nets
  # Handle all p2s nets that are not connected
  connect_nets_to_gnd $unused_p2s
  save_checkpoint "${static_system_output}/${static_system_name}_p2s_gnd.dcp"

  # Restore design constraints
  partition_nets::restore_design_constraints $inst_partialarea
  save_checkpoint "${static_system_output}/${static_system_name}_lockpins_restored.dcp"

  route_design -physical_nets

  # Restore clock logic
  drt::restore_clock_logic
  save_checkpoint "${static_system_output}/${static_system_name}_restore_clock_logic.dcp"

  # Fix all nets before routing to prevent any route optimizations by Vivado router
  set_property IS_ROUTE_FIXED 1 [get_nets -hierarchical]

  # Restore proxy logic s2p"
  #anchor_logic::restore_s2p_anchor_LUTs $unused_s2p $inst_partialarea
  #save_checkpoint "${static_system_output}/${static_system_name}_restore_s2p_anchor_LUTs.dcp"

  ##########################################################################################
  # Do some post fixes
  drt::postfix_script

  ##########################################################################################
  # When a module turns into a blackbox we must delete it, else it will be a DRC error
  drt::remove_blackbox
  save_checkpoint "${static_system_output}/${static_system_name}_remove_blackbox.dcp"


  ##########################################################################################
  # Clear any nets with status conflicts
  drt::clear_conflicts_nets

  ##########################################################################################
  # Do a final route of the design
  drt::route_design_final

  ##########################################################################################
  # Write the final checkpoint and bitstream
  drt::write_checkpoint_final $static_system_output $static_system_name
  drt::end_script
}

proc drt::route_design_final {} {
  # Certain operations on nets and logic mark some (physical) nets as unrouted.
  set unrouted [get_nets -filter ROUTE_STATUS==UNROUTED]
  if {[llength $unrouted] > 0} {
    logger.info "There are [llength $unrouted] net(s) with status UNROUTED: $unrouted"
  }
  route_design
}

proc drt::write_checkpoint_final {output name} {
  set dcp "${output}/${name}_final.dcp"
  save_checkpoint $dcp
  set bitfile [string map {".dcp" ".bit"} $dcp]
  write_bitstream -force $bitfile
}

proc drt::postfix_script {} {
  # Do some post fixes
  set postfix_script "$drt::static_system_input/postfix.tcl"
  if {[file exist $postfix_script] != 0} {
    logger.info "Execute post fix script..."
    source $postfix_script
  }

  save_checkpoint "${drt::static_system_output}/${drt::static_system_name}_after_postfix.dcp"
}

proc drt::start_script {} {
  variable static_system_input
  logger.info "============================================================================"
  logger.info "Start of script [file tail [info script]]."
  logger.info "input: $static_system_input"
}

proc drt::end_script {} {
  logger.info "============================================================================"
  logger.info "End of script [file tail [info script]]."
}

proc drt::remove_blackbox {} {
  variable modules_config

  # When a module turns into a blackbox we must delete it, else it will be a DRC error
  foreach module $modules_config {
    set cells [get_cells $module/*]
    foreach cell $cells {
      if {[get_property IS_BLACKBOX $cell] == 1} {
        logger.info "Removing blackbox $cell."
        remove_cell $cell
        remove_cell $module
        break;
      }
      break; # should be one cell
    }
  }
}

proc drt::check_any_unplaced_cells {} {
  # Get all unplaced cells, ignore power nets
  # Ignore unplaced FDRE cells which where placed by GoAhead tool
  set cells [get_cells -hierarchical -quiet -filter {STATUS == "UNPLACED" && (REF_NAME != "VCC" && REF_NAME != "GND" || (REF_NAME=="FDRE" && NAME~="SLICE_*"))}]
  if {[llength $cells] > 0} {
    logger.error "There are [llength $cells] unplaced cell(s). Following cells have status 'UNPLACED': $cells"
  } else {
    logger.info "No unplaced cells found."
  }
}

proc drt::clear_nets_not_used {} {
  variable interface_map
  set keys_to_delete [list]

  # Clear the routes
  dict for {from_net to_net} $interface_map {
    if {$to_net == "" || $to_net == "CLEAR_ROUTE"} {
      clear_route [get_nets $from_net]
      lappend keys_to_delete $from_net
    }
    if {[string first "REMOVE_INTERFACE" $to_net] != -1} {
      lappend keys_to_delete $from_net
    }
  }

  # Delete the nets
  foreach key $keys_to_delete {
    logger.info "Deleted net from interface map: $key"
    dict unset interface_map $key
  }
}

proc drt::get_last_error {} {
  logger.info $::errorInfo
}

proc drt::vivado_settings {} {
  if {[version -short] != "2018.3"} { logger.warning "Untested in this version of Vivado: [version -short]"}
  set_param messaging.defaultLimit 2000
  set_param tcl.collectionResultDisplayLimit 0
}

proc drt::clear_conflicts_nets {} {
  # Merging nets issue, a net alias is required for some nets.
  # In Vivado when we select a net in the Device view, there is an Aliases tab in the Net Properties window.
  # It show all the nets that have the same route and pins.
  # Via TCL we can get this info via the command get_nets -segments.
  # At this point we do not know how to create such alias from TCL.
  # So for now the only option is to clear the ROUTE property of the net.

  #set nets_conflicts [get_nets * -filter {ROUTE_STATUS==CONFLICTS && NAME=~*p2s*} -hierarchical]
  set nets_conflicts [get_nets -hierarchical -filter {ROUTE_STATUS==CONFLICTS && NAME=~*p2s*} *]

  foreach net $nets_conflicts {
    set_property ROUTE {} [get_nets $net]
    logger.warning "Net '$net' has status CONFLICTS and its route has been cleared."
  }
}

proc drt::get_dcp_files {static_system_input modules_input upvar_staticsytem_dcp upvar_staticsystem_name upvar_modules_dcp} {
  upvar $upvar_staticsytem_dcp staticsystem_dcp
  upvar $upvar_staticsystem_name staticsystem_name
  upvar $upvar_modules_dcp modules_dcp

  set staticsystem_dcp [lindex [glob -nocomplain "${static_system_input}/*.dcp"] 0]
  set staticsystem_name [string map {".dcp" ""} [file tail $staticsystem_dcp]]
  set modules_dcp [glob -nocomplain "${modules_input}/*.dcp"]

  # Check if the input DCP files exists
  if {$staticsystem_dcp == ""} {logger.error "The static system DCP file does not exist."}
  if {$staticsystem_name == ""} {logger.error "The static system name is invalid."}
  if {[llength $modules_dcp] < 1} {logger.error "No module DCP files found."}
}

proc drt::preserve_anchor_logic {} {
  anchor_logic::get_anchor_logic $drt::inst_partialarea
  slvar::save anchor_logic::v::anchor_luts
  slvar::save anchor_logic::v::anchor_luts_pins_gnd
  slvar::save anchor_logic::v::anchor_luts_nets
  slvar::save anchor_logic::v::anchor_routes
  set lut_pin_assignments [get_lut_pin_assignments]
  slvar::save lut_pin_assignments
}

##########################################################################################
# Prepare the modules for placing
# - get the ports from the partial area
# - place them in the netlist of the modules
# - assign the LOCK_PINS property for each LUT cell
##########################################################################################
proc drt::prepare_modules_for_placing {} {
  set virtual_ports [partition_nets::get_virtual_ports $drt::static_system_input_dcp $drt::inst_partialarea]

  #Copy modules to the output directory
  foreach module_config $drt::modules_config {
    set input "${drt::modules_input}/${module_config}.dcp"
    set output "${drt::modules_output}/${module_config}.dcp"
    if {![file exist $input]} {
      logger.warning "Warning: DCP file does not exist: $input."; continue;
    } else {
      file copy -force $input $output
    }
  }

  # First remove the unused interface
  partition_nets::remove_unused_interface_module $drt::modules_config $drt::interface_map $drt::inst_module $drt::modules_output

  partition_nets::add_virtual_ports_and_lock_pin_constraints $drt::modules_config $drt::inst_module $drt::modules_output $drt::modules_output $virtual_ports
}

proc drt::prepare_staticdesign_for_placement {} {
  set pblock [get_pblocks -of_objects [get_cells $drt::inst_partialarea]]
  set_property PARENT ROOT $pblock
  set cell [get_cells -of_objects $pblock]
  set nets [get_nets -of_objects $cell -filter "TYPE !~ *CLOCK"]
  set_property IS_ROUTE_FIXED 0 [get_nets -hierarchical]
  set_property IS_ROUTE_FIXED 1 $nets
  set_property HD.partition 1 $cell
  update_design -cells $cell -black_box
}

proc drt::remove_cell_partial_area {} {
  set nets [get_nets -of_objects [get_cells $drt::inst_partialarea]]
  set_property DONT_TOUCH 0 [get_nets $nets]
  remove_cell $drt::inst_partialarea
}

proc drt::restore_clock_logic {} {
  if {$drt::internal_module_clk != "" && $drt::external_module_clk != ""} {
    partition_nets::restore_clock_nets $drt::modules_config $drt::inst_module $drt::internal_module_clk $drt::external_module_clk
    save_checkpoint "${drt::static_system_output}/${drt::static_system_name}_restore_clocknets.dcp"
  }
}

proc drt::read_mapping {mapping_file} {
  set script $mapping_file
  unset -nocomplain mapping_config
  unset -nocomplain interface_map
  unset -nocomplain unused_p2s
  unset -nocomplain unused_sp2
  unset -nocomplain internal_module_clk
  unset -nocomplain external_module_clk
  unset -nocomplain inst_partialarea

  if {[file exist $script]==0} {logging.error "Mapping script not found: ${script}."}
  source $script

  if {![info exists mapping_config]}   {logging.error "Variable 'mapping_config' not set."}
  if {![info exists modules_config]}   {logging.error "Variable 'modules_config' not set."}
  if {[dict size $mapping_config] < 1} {logging.error "Variable 'mapping_config' is empty."}
  if {[llength $modules_config] < 1}   {logging.error "Variable 'modules_config' is empty."}
  if {![info exists unused_p2s]} {set unused_p2s [list]}
  if {![info exists unused_s2p]} {set unused_s2p [list]}
  if {![info exists internal_module_clk]} {set internal_module_clk ""}
  if {![info exists external_module_clk]} {set external_module_clk ""}
  if {![info exists inst_partialarea]} {set inst_partialarea "inst_PartialArea"}

  #set interface_map [dict merge [dict create] $mapping_config]

  set drt::modules_config $modules_config
  set drt::interface_map [dict merge [dict create] $mapping_config]
  set drt::unused_s2p $unused_s2p
  set drt::unused_p2s $unused_p2s
  set drt::internal_module_clk $internal_module_clk
  set drt::external_module_clk $external_module_clk
  set drt::inst_partialarea $inst_partialarea
}
