########################################################################################
# Namespace for holding anchor logic
########################################################################################
namespace eval anchor_logic {

  # Use namespace for holding variables. This eleminates variable declaration in procs.
  namespace eval v {
    variable anchor_luts [dict create]
    variable anchor_luts_pins_gnd [dict create]
    variable anchor_routes [dict create]
    variable anchor_luts_nets [dict create]
  }

  proc create {} {
    puts "constructor"
  }
}

# Save a dict with the anchor LUTs in the partition
proc anchor_logic::get_anchor_logic {inst_partialarea} {
  set cells [get_cells -filter "PRIMITIVE_GROUP==LUT && PARENT==$inst_partialarea" -hierarchical]
  foreach cell $cells {
    set name [get_property NAME [get_cells $cell]]
    set loc [get_property LOC [get_cells $cell]]
    set bel [get_property BEL [get_cells $cell]]
    set refname [get_property REF_NAME [get_cells $cell]]
    set init [get_property INIT [get_cells $cell]]
    set pins [get_pins -of_objects [get_cells $cell] -filter "DIRECTION==IN"]
    set pins_gnd [list]

    # get all pins connected to GROUND
    foreach pin $pins {
      if {[llength [get_nets -of_objects [get_pins $pin] -quiet -filter "TYPE==GROUND"]] == 1} {
        lappend pins_gnd $pin
      }
    }

    # get all nets and route
    set nets [get_nets -of_objects [get_cells $cell] -filter "TYPE!=GROUND"]
    dict append v::anchor_luts_nets $cell $nets
    foreach net $nets {
      dict append v::anchor_routes $net [get_property ROUTE [get_nets $net]]
    }

    dict append v::anchor_luts_pins_gnd $cell $pins_gnd
    dict append v::anchor_luts $cell [list $loc $bel $refname $init $pins]
  }
}




proc f_contains {haystack needle} {return [expr {[string first $needle $haystack] != -1}]}
proc f_contains_s2p {haystack} {return [f_contains $haystack "s2p"]}
proc f_list_contains_item {litems item} {return [expr {$item in $litems}]}
proc f_list_contains_any_items_substring {llist items} {
  foreach item $items {
    logger.debug "f_list_contains_any_items_substring $item"
    if {[f_contains $llist $item]} {return 1}
  }
  return 0
}

# TODO must be implemented
proc is_empty_partial_area {k v} {return 1}

proc anchor_logic::restore_s2p_anchor_LUTs {lnets inst_partialarea} {

  # End proc if we have not anchor LUT nets to restore
  if {[llength $lnets] == 0} {return}

  # Try to load the anchor LUT information
  set loaded 0
  incr loaded [slvar::load anchor_logic::v::anchor_luts]
  incr loaded [slvar::load anchor_logic::v::anchor_luts_nets]
  incr loaded [slvar::load anchor_logic::v::anchor_luts_pins_gnd]
  incr loaded [slvar::load anchor_logic::v::anchor_routes]

  if {$loaded != 4} { logger.error "Error: anchor logic could not be loaded from file."}

  # Remove [*]
  # TODO when we have [1]
  set nets [lmap i $lnets { string map {"\[*\]" ""} $i }]

  logger.debug "nets: $nets"

  set anchor_luts_nets [dict filter $anchor_logic::v::anchor_luts_nets script {key value} {expr {
    [is_empty_partial_area $key $value] && [f_list_contains_any_items_substring $value $nets]
  }}]


  if {[llength $anchor_luts_nets] == 0} {
    logger.info "Info: no anchor logic to restore."
    return
  }

  # TODO remove this requirement:
  create_cell $inst_partialarea -black_box -reference PartialArea

  set ground_pins [list]

  # Restore s2p nets. Either to LUTs or power nets
  dict for {lut properties} $anchor_logic::v::anchor_luts {
    lassign $properties slice bel type init pins

    if {[dict exist $anchor_luts_nets $lut]} {
      #logger.debug "Restore LUT: $lut"
      set nets [dict get $anchor_luts_nets $lut]
      lassign [::struct::list filter $nets f_contains_s2p] net
      set route [dict get $anchor_logic::v::anchor_routes $net]
      set pins_gnd [dict get $anchor_logic::v::anchor_luts_pins_gnd $lut]
      lappend ground_pins {*}$pins_gnd
      set net [lindex [split $net "/"] end]

      create_cell -reference $type $lut
      set_property INIT 64'hABCDABCDABCDABCD [get_cells $lut]
      place_cell $lut $slice/$bel
      set dummy_net "dummy_${net}"
      create_net $dummy_net

      logger.debug "net: $net, lut: $lut"

      #maybe create the pins?
      create_pin -direction IN "${lut}/I0"
      create_pin -direction IN "${lut}/I1"
      create_pin -direction IN "${lut}/I2"
      create_pin -direction IN "${lut}/I3"
      create_pin -direction IN "${lut}/I4"
      create_pin -direction IN "${lut}/I5"
      create_pin -direction OUT "${lut}/O"

      connect_net -net [get_nets -hierarchical $net] -objects [get_pins ${lut}/I3] -hierarchical -verbose
      connect_net -net [get_nets $dummy_net] -objects [get_pins ${lut}/O] -hierarchical -verbose

      set_property LOCK_PINS {I3:A4} [get_cells $lut]
      set_property IS_BEL_FIXED 0 [get_cells $lut]
      set_property IS_LOC_FIXED 0 [get_cells $lut]
      set_property ROUTE {} [get_nets -hierarchical $net]
      set_property ROUTE $route [get_nets -hierarchical $net]
# why set 0 above?

      set_property IS_BEL_FIXED 1 [get_cells $lut]
      set_property IS_LOC_FIXED 1 [get_cells $lut]
      set_property IS_ROUTE_FIXED 1 [get_nets -hierarchical $net]
    }
  }


  # should we connect all non used inputs to GND?
  # yes, else we get ERROR: [Designutils 20-2553] Could not convert equation O6=( on site SLICE_X56Y99
  set gnd_cell "anchor_luts_gnd_cell"
  set gnd_net "anchor_luts_gnd_net"
  create_net $gnd_net
  create_cell -reference GND $gnd_cell
  lappend ground_pins "${gnd_cell}/G"
  connect_net -net [get_nets $gnd_net] -objects [get_pins $ground_pins] -hierarchical

  set partial_nets [get_nets -quiet -filter ROUTE_STATUS==PARTIAL -hierarchical]
  if {[llength $partial_nets] > 0} {
    disconnect_net -objects [get_pins -of_objects [get_nets -filter ROUTE_STATUS==PARTIAL -hierarchical]]
    logger.warning "Warning: The following partial nets could not be restored: $partial_nets"
  }

  set antenna_nets [get_nets -quiet -filter ROUTE_STATUS==ANTENNAS -hierarchical]
  if {[llength $antenna_nets] > 0} {
    set_property ROUTE {} [get_nets $antenna_nets -hierarchical]
    logger.warning "Warning: The following antenna nets could not be restored: $antenna_nets"
  }


  logger.debug "Restore LUTs done."
}