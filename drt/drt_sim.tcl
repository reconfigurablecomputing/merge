##########################################################################################
# Design reconstruction simulation
#
#
##########################################################################################

namespace eval drt_sim {

    # Export
    namespace export create
    namespace export run

    variable path_to_vivado          "/home/jeroen/Xilinx/Vivado/2018.3"
    variable path_to_script          [file dirname [file normalize [info script]]]
    variable build_dir               "${path_to_script}/.build"
}

proc drt_sim::create {path_to_vivado} {
    set drt_sim::path_to_vivado $path_to_vivado
}

proc drt_sim::run {dcp_file xsim_arg_file top_unitname} {

    set build_dir $drt_sim::build_dir
    set path_to_script $drt_sim::path_to_script
    set path_to_vivado $drt_sim::path_to_vivado

    # Clean up
    file delete -force $build_dir
    file mkdir $build_dir
    file copy $xsim_arg_file $build_dir/xsim_arguments.tcl

    open_checkpoint $dcp_file
    cd $build_dir
    puts -nonewline "Changing Directory to "; puts [pwd];

    # for vhdl note that:
    # write_vhdl does not support mode 'timesim'
    # write_vhdl does not support 'sdf_anno'
    # '-force' to overwrite existing file
    # write_vhdl time_sim.vhd -force

    # for verilog
    write_verilog -mode timesim -nolib -sdf_anno true -force -sdf_file $build_dir/time_sim.sdf $build_dir/time_sim.v
    write_sdf -mode timesim -process_corner slow $build_dir/time_sim.sdf -force

    # The xvlog command parses the source file(s) and stores the parsed dump into a HDL library on disk.
    set XVLOG $path_to_vivado/bin/xvlog
    exec $XVLOG $build_dir/time_sim.v --incr --relax
    # exec $XVLOG time_sim_tb.v

    set GLBL $path_to_vivado/data/verilog/src/glbl.v
    exec $XVLOG $GLBL


    exec xelab --incr --debug typical --relax --mt 8 --maxdelay -L xil_defaultlib -L simprims_ver -L secureip -L unisims_ver -L unimacro_ver -transport_int_delays --pulse_r 0 --pulse_int_r 0 --pulse_e 0 --pulse_int_e 0 glbl $top_unitname -s $top_unitname -log $build_dir/elaborate.log

    exec xsim $top_unitname -gui -tclbatch {xsim_arguments.tcl} -log $build_dir/simulate.log
}

