########################################################################################
# dict append values
# append values to a dict, where values of each key is treated as a list.
# Arguments:
#   upvar_dict: the dictionary variable
#   key: the dictionary key
#   valus: value(s) as a list
# Result:
#
########################################################################################
proc dict_append_values { upvar_dict key values } {
  upvar $upvar_dict d
  if { [dict exist $d $key ]} {
    set current_values [dict get $d $key]
    set new_values [concat $current_values $values]
    dict set d $key $new_values
  } else {
    dict append d $key $values
  }
 }

########################################################################################
# dict append keys
# Append a list of keys to a dict.
#
# Arguments:
#   upvar_dict: the dictionary variable
#   keys: a list of keys to add to the dictionary
#
# Result:
#  The keys are added to upvar_dict
########################################################################################
proc dict_append_keys { upvar_dict keys } {
  upvar $upvar_dict d
  foreach key $keys {
    dict append d $key ""
  }
 }

########################################################################################
# dict filter where value is list
# Filter a dict where the filter_value is a item of the value from the key.
#
# Arguments:
#   upvar_dict: the dictionary variable
#   filter_value: value to filter on
#
# Result:
#  The filtered dict
########################################################################################
proc dict_filter_where_value_is_list { upvar_dict filter_value} {
  upvar $upvar_dict d
  set df [dict create]
  set filter [string map { "\[" "\\[" "\]" "\\]"} $filter_value]
  set filter "*${filter}*"

  dict for {key values} $d {
    set result [lsearch -inline -all $values $filter]
    if {[llength $result] > 0} {
      dict append df $key $values
    }
  }

  return $df
}


########################################################################################
# dict try get value
#
# Arguments:
#   upvar_dict: the dictionary variable
#
# Result:
#
########################################################################################
proc dict_try_get_value { upvar_dict key upvar_value} {
  upvar $upvar_dict d
  upvar $upvar_value value

  if {[dict exist $d $key]} { set value [dict get $d $key]; return 1;}
  return 0
}


########################################################################################
# dict_first_pair
#
# Arguments:
#   upvar_dict: the dictionary variable
#
# Result:
#  The first key/value pair from the dictionary
########################################################################################
proc dict_first_pair {upvar_dict} {
  upvar $upvar_dict d
  dict for {k v} $d { return [list $k $v] }
  return [list]
}

proc dict_first_value {upvar_dict} {
  upvar $upvar_dict d
  dict for {k v} $d { return $v }
  error "Dictionary is empty"
}

proc dict_first_key {upvar_dict} {
  upvar $upvar_dict d
  dict for {k v} $d { return $k }
  error "Dictionary is empty."
}


########################################################################################
# isDict
# ref: https://wiki.tcl-lang.org/page/isDict
# Arguments:
#   upvar_dict: the dictionary variable
#
# Result:
#
########################################################################################
proc isDict value {
  expr {![catch {dict size $value}]}
}
