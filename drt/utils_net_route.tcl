########################################################################################
# utils net and route
########################################################################################


########################################################################################
# is net direction input
# Arguments:
#  net: the net name
# Results:
#  1 if the net is an input net, 0 otherwise
########################################################################################
proc is_net_dir_in { net } {
  set dir [get_property DIRECTION [get_pins -of_objects [get_nets $net] -filter "NAME=~*$net*"]]
  if {$dir == "IN"} { return 1}
  return 0
}


########################################################################################
# is net direction output
# Arguments:
#  net: the net name
# Results:
#  1 if the net is an output net, 0 otherwise
########################################################################################
proc is_net_dir_out { net } {
  set dir [get_property DIRECTION [get_pins -of_objects [get_nets $net] -filter "NAME=~*$net*"]]
  if {$dir == "OUT"} { return 1}
  return 0
}


########################################################################################
# get route list from route string
# Arguments:
#
# Results:
#
########################################################################################
proc get_route_list_from_route_string {route_string} {
  set route_list [regexp -all -inline {\S+} $route_string]
  return $route_list
}


########################################################################################
# get route string from route list
# Arguments:
#
# Results:
#
########################################################################################
proc get_route_string_from_route_list {route_list} {
  set route_string [join $route_list " "]
  return $route_string
}


########################################################################################
# net split by the last /
# Arguments:
#   net_name
# Results:
#   the last part of the net name
########################################################################################
proc net_split {net_name} {
   return [lindex [split $net_name "/"] end]
}


########################################################################################
# get absolute routestring from nets
#
# Arguments:
#  list of nets
# Results:
#  a dictionary of nets (key) and absolute route string (value)
########################################################################################
proc get_abs_route {nets} {
  return [get_absolute_routestring_from_nets_dict $nets]
}


########################################################################################
# get absolute route string from a single net
#
# Arguments:
#  net name, retrieved via get_nets
# Results:
#  the route string that includes the tile information
########################################################################################
proc get_abs_route_net {net} {
  lappend nets $net
  set routestring [get_absolute_routestring_from_nets_dict $nets]
  return [dict_first_value routestring]
}


########################################################################################
# get absolute routestring from nets
#
# Arguments:
#  list of nets
# Results:
#  a dictionary of nets (key) and absolute route string (value)
########################################################################################
proc get_absolute_routestring_from_nets_dict {nets} {
  set dict_nets_routestring [dict create]
  set ignore_list { "\{" "\}" }
  foreach net $nets {
    set pips [get_pips -downhill -of_objects [get_nets $net]]
    set nodes [get_nodes -of_objects [get_nets $net]]
    set route [get_route_list_from_route_string [get_property ROUTE [get_nets $net]]]
    set absolute_route [list]
    set list_tilewire [get_tilewire_from_pip $pips]
    set wire_index 0
    set original_wire ""
    set trigger 0
    set i 0
    set start_with_cardinal_wire 0

    #if {$net == "inst_AES/AESRound/x0y1_s2p_w[106]"} {set trigger 1}

    # Try to match each wire with its tile
    foreach wire $route {

      if {$trigger} {logger.debug "1. wire $wire"}

      # The current wire has cardinal info and no tile info
      if {![is_absolute_wire $wire]} {
        if {[is_cardinal_wire $wire]} {

            if {$i == 1} {
              set start_with_cardinal_wire 1
            } else {
              # The wire start with an angle bracket index e.g. <0>
              set angle_bracket 0
              if {[regexp {([<][0-9]+[>])?([A-Z0-9_<>]+)} $wire fullmatch index wr]} {
                if {[llength $index] > 0} {
                  # Continue with the angle bracket index removed. They do not appear in the wire/node list.
                  set original_wire $wire
                  set wire $wr
                  set angle_bracket 1
                  if {$trigger} {logger.debug "The wire start with an angle bracket index e.g. <0>"}
                }
              }

              if {$trigger} {logger.debug "list_tilewire $list_tilewire"}

              # Get the tile inforation from the pip list
              set idx [lsearch $list_tilewire *$wire]

              # Use the tile/wire combination when it exist the pip list.
              if {$idx >= 0} {
                set wire [lindex $list_tilewire $idx]
                set list_tilewire [lreplace $list_tilewire $idx $idx]

                # Restore original wire if we had a wire with angle bracket index.
                # We could have a common node on the node that has an angle bracket.
                # Vivado accepts a node with tile information like tile/<0>node in the route string
                # Note that the get_nodes command does not accept this format nor do they appear as a result of the command.
                if {$angle_bracket} {
                  set wire "$index$wire"
                  if {$trigger} {logger.debug "The original wire: '$original_wire' wire with tile info: '$wire'"}
                }
              } else {
                logger.error "Node not found in net '$net' and wire '$wire' at index $i/[llength $route] and route\n $route"
              }

              if {$trigger} {logger.debug "3. wire $wire"}

            }

        } else {
          if {[lsearch $ignore_list $wire] == -1} {
            set prev_wire [lindex $absolute_route $wire_index-1]
            if {[is_absolute_wire $prev_wire]} {

              # Try to resolve wires like: IMUX_L11, CLBLM_M_A4...
              # Todo resolve wires like LV0, LV_L0.
              # They appear in the route string, but have slightly different names in the node list e.g. LV_L18
              set tile [get_tile_from_tilewire $prev_wire]
              set pip [get_pips -quiet -downhill -of_objects [get_nodes -quiet -of_objects [get_tiles $tile] $prev_wire] *$wire*]
              if {[llength $pip]==1} {
                set tile [get_property TILE [get_pips $pip]]
                set wire $tile/$wire
              } else {
                logger.warning "Could not resolve wire '$wire' in net '$net'."
              }
            }
          }
        }
      }
      lappend absolute_route $wire
      incr wire_index 1
      incr i
    }

    if {$start_with_cardinal_wire} {
      set wire [lindex $absolute_route 1]
      set found_first 0
      foreach node $nodes {
        if {[lindex [split $node "/"] end] == $wire} {

          if {[lsearch $absolute_route $node] == -1} {
            set absolute_route [lreplace $absolute_route 1 1 $node]
            set found_first 1
            break
          }
        }
      }
      if {!$found_first} {logger.error "Starting node not found."}
    }
    dict append dict_nets_routestring $net $absolute_route
  }
  return $dict_nets_routestring
}


########################################################################################
# get tilewire from pip
# place all pips in a list formated ike <tile>/<wire> that have this <tile></><from wire><wire>
# example: INT_L_X32Y90/INT_L.SS6END0->>SS6BEG0  => INT_L_X32Y90/SS6BEG0
#
# Arguments:
#
# Result:
#  list of tile/wire combinations
########################################################################################
proc get_tilewire_from_pip {pip_list} {
  set tilewire [list]
  foreach pip $pip_list {
    if {[regexp {(.*)(\/)(.*)(->>)(.*)} $pip fullmatch tile sep1 from sep2 to]} {
      lappend tilewire "$tile/$to"
    }
  }
  return $tilewire
}


########################################################################################
# get_unique_tilewire
#
# Arguments:
#
########################################################################################
proc get_unique_tilewire {nodes upvar_wire} {
  upvar $upvar_wire wire
  set nodes [lsearch -all -inline $nodes *$wire]
  if {[llength $nodes] == 1} {
    lassign $nodes wire
    return 1
  } else {
    return 0
  }
}


########################################################################################
# get_net_intersections
#
# Arguments:
#   dict_net1: dictionary, where key is the net name, and value is the route string (as a list)
#   dict_net2: dictionary, where key is the net name, and value is the route string (as a list)
# Result:
#   dict_overlap containing the common node that indicates the intersection
########################################################################################
proc get_net_intersections {dict_net1 dict_net2 dict_interface_map {first_node 0} } {
  set intersections [dict create]
  set node ""
  set count 0
  set i 0
  set error_count 0
  set error_nets [list]
  set debug 0

  dict for {from_net from_route} $dict_net1 {
    # Check if interface mapping exist
    if [dict exist $dict_interface_map $from_net] {
      set to_nets [dict get $dict_interface_map $from_net]
      foreach to_net $to_nets {
        # and the net exist in the target net
        if [dict exist $dict_net2 $to_net] {
          set to_route [dict get $dict_net2 $to_net]

          if {[catch {set idx [get_node_intersect $from_route $to_route node $first_node $debug]} errmsg]} {
            logger.info "$i/[dict size $dict_net1] from net $from_net, to net $to_net."
            logger.info "to route: $to_route"
            logger.info "ErrorMsg: $errmsg"
            logger.info "ErrorCode: $::errorCode"
            logger.error "ErrorInfo:\n$::errorInfo\n"
          }

          if {$idx < 0} {
            logger.info "to_nets: $to_nets"
            logger.warning "No common node found between nets: \n${from_net}\n${to_net}. $i/[dict size $dict_net1]"
            lappend error_nets $from_net
            incr error_count 1
          } else {
            dict_append_values intersections $from_net $node
            incr count 1
          }
        }

      }
    }
    incr i 1
  }

  # Check the result
  if {$count == 0} {
    logger.info "dict_interface_map $dict_interface_map"
    logger.error "No common nodes found for [dict size $dict_net1] net(s): [dict keys $dict_net1]"
  }
  if {$error_count > 0} {logger.error "No common node found for $error_count net(s): $error_nets"}

  logger.debug "intersections:\r\n[dict values $intersections]"
  return $intersections
}

proc get_node_index_ignore_angle_brackets {nodes node_to_find} {
  set node_to_find_stripped [lindex [split $node_to_find ">"] end]
  set i 0
  foreach node $nodes {
    set n [lindex [split $node ">"] end]
    if {$node_to_find_stripped == $n} {return $i}
    incr i 1
  }
  return -1
}


########################################################################################
# combine two nets based on the common node in net1 and net2
#
# Arguments:
#
# Result:
#
########################################################################################
proc combine_net {net1 net2 node {net1_remove_till_end 0}} {
  set n1_node_idx [get_node_index_ignore_angle_brackets $net1 $node]
  set n1_bracket_idx [expr [lsearch -start $n1_node_idx $net1 \} ] -1 ]
  set n2 [lreplace [lreplace $net2 end end ] 0 0]; # Remove the outer brackets
  set n2_node_idx [get_node_index_ignore_angle_brackets $n2 $node]

  if {($n1_node_idx == -1)} {logger.error "In proc combine_net: The node '$node' was not found in net1."}
  if {($n2_node_idx == -1)} {logger.error "In proc combine_net: The node '$node' was not found in net2."}

  set n2_pre_node [lindex $n2 [expr {$n2_node_idx -1}]] ; # get the item before the node
  set ob_found 0
  if {$n2_pre_node == "\{" } {set ob_found 1}

  # Remove any leading nodes before the intersection node
  set n2 [lrange $n2 [get_node_index_ignore_angle_brackets $n2 $node] end]

  # Insert an additional open brace at the begin
  if {$ob_found} { set n2 [linsert $n2  0 "\{"]}

  if {$net1_remove_till_end} {
    # Remove all nodes starting from node to the closing bracket
    set n1_replaced [lreplace $net1 $n1_node_idx end-1]
  } else {
    # Remove all nodes starting from node to the first closing bracket
    set n1_replaced [lreplace $net1 $n1_node_idx $n1_bracket_idx]
  }

  # Insert the net
  set net [list_insert_sublist_at_index $n1_replaced $n2 $n1_node_idx]

  if {$net1_remove_till_end} {
    # If the node in net1 is in a branch, we must also remove a open brace
    # Try to find the first open brace, exclude the first brace.
    for {set i $n1_node_idx} {$i > 0} {incr i -1} {
      set n [lindex $net $i]
      if { $n == "\{" } {
        set net [lreplace $net $i $i]
        #logger.info "Removed open brace in net at index $i"
        break;
      } elseif { $n == "\}" } {break;}
    }
  }

  return $net
}

########################################################################################
# combine nets
#
# Arguments:
#   dict_nodes: dictionary containing the net as key and the node as value
# Result:
#  returns a dict with the combined nets
########################################################################################
proc combine_nets {dict_net1 dict_net2 dict_nodes dict_map {net1_remove_till_end 0}} {
  set combined [dict create]
  set count 0
  set debug 1
  set trigger 0
  set trigger_val "module_SubBytes_config2/inst_Module/SB_OUT[14]"

  dict for {net1 net1_route} $dict_net1 {
    set trigger 0
    if {$debug==1} {if {$net1==$trigger_val} {set trigger 1}}

    if {[dict exist $dict_map $net1]} {
      set nets2 [dict get $dict_map $net1]
      if {![dict exist $dict_nodes $net1]} {logger.error "No nodes exist for net '$net1'."}

      set nodes [dict get $dict_nodes $net1]
      if {[llength $nets2] != [llength $nodes]} {logger.error "The number of nodes do not match the number of nets."}

      foreach net2 $nets2 node $nodes {
        if {[dict exist $dict_net2 $net2]} {
          set net2_route [dict get $dict_net2 $net2]

          # Check if net is already combined once
          if {[dict_try_get_value combined $net1 r1]} {
            set route [combine_net $r1 $net2_route $node $net1_remove_till_end]
            if {$trigger} {
              logger.debug "Update route of net1: $net1, node: $node"
              logger.debug "1: $net1, route: $r1"
              logger.debug "2: $net2, route: $net2_route"
              logger.debug "combined route : $route"
            }
          } else {
            set route [combine_net $net1_route $net2_route $node $net1_remove_till_end]
            if {$trigger} {logger.debug "New route net1: $net1, node: $node, route: $route"}
          }
          dict set combined $net1 $route
          if {[is_balanced $route] != 1} {

            logger.error "Braces in route of net $net1 are not balanced."
          }
          incr count 1
        } else {
          logger.warning "The mapped net was not found: '$net2'."
        }
      }
    }
  }

  if {$count == 0} {logger.warning "There are no nets to be combined."}

  return $combined
}

########################################################################################
# is_balanced
# simple check if braces are balanced
# Arguments:
#
# Result:
#
########################################################################################
proc is_balanced {lroute} {
  set op [lsearch -all $lroute \{]
  set cp [lsearch -all $lroute \}]
  if {[llength $op] == [llength $cp]} {return 1}
  return 0
}


########################################################################################
# Is absolute wire
#
# if a "/" is present in the string, it has tile information
########################################################################################
proc is_absolute_wire {wire} {
  if {[string first "/" $wire] >= 0} {
    return 1
  } else {
    return 0
  }
}

########################################################################################
#
# Arguments:
#
# Results:
#
########################################################################################
proc get_tile_from_tilewire {tilewire} {
  set list_tile_wire [split $tilewire "/"]
  return [lindex $list_tile_wire 0]
}


########################################################################################
#
# Arguments:
#
# Results:
#
########################################################################################
proc get_xy { wire upvar_xy } {
  upvar $upvar_xy xy
  set x 0
  set y 0
#  set reg_xy_pattern {([X][0-9][0-9][0-9]?)([Y][0-9][0-9][0-9]?)}
  set reg_xy_pattern {([X][0-9]+)([Y][0-9]+)}
  set match [regexp $reg_xy_pattern $wire x_y x y ]
  set x [string trim $x "X"]
  set y [string trim $y "Y"]
  set xy [list $x $y ]
  return $match
}


########################################################################################
#
# Arguments:
#
# Results:
#
########################################################################################
proc is_cardinal_wire {wire} {
  set reg_wire_pattern {([A-Z][A-Z])([0-9])([A-Z][A-Z][A-Z])([_1-9]?)([A-Z]?)([0-9]?)}
  set match [regexp $reg_wire_pattern $wire all dir nr b_e wire_nr c nr_c ]
  return $match;
}

########################################################################################
#
# Arguments:
#
# Results:
#
########################################################################################
proc get_cardinal_from_wire {wire upvar_cardinal} {
  upvar $upvar_cardinal cardinal
  set reg_wire_pattern {([A-Z][A-Z])([0-9])([A-Z][A-Z][A-Z])([_1-9]?)([A-Z]?)([0-9]?)}
  set match [regexp $reg_wire_pattern $wire all dir nr b_e wire_nr c nr_c ]
  set known_dir [list NN NL NR NE NW EE ER EL WW WR WL SS SL SR SE SW ]

  if {$match} {
    set cardinal [list $dir $nr $b_e $wire_nr $c $nr_c]
    if {[lsearch $known_dir $dir] < 0} {
      puts "Cardinal direction ${dir} is not known"
      set match 0
    }
  } else {
    set cardinal [list "" 0 "" 0 "" 0]
  }
  return $match
}


########################################################################################
#
# Arguments:
#
# Results:
#
########################################################################################
proc xy_displacement_from_wire {wire} {
  set cardinal [list]
  set xy [list 0 0]
  set is_cardinal_wire [get_cardinal_from_wire $wire cardinal]

  if {$is_cardinal_wire} {
    set dir [lindex $cardinal 0]
    set nr [lindex $cardinal 1]
    set c [lindex $cardinal 4]
    set y 0
    set x 0

    # when we have ER1BEG_S0
    # this is guessed, there is no documentation about it.
    if {$nr == 1} {
      switch $c {
        N {
          set y -1
        }
        S {
          set y 1
        }
        default {
          set y 0
        }
      }
    }

    switch $dir {
      NL -
      NR -
      NN {
        set nr [ expr $nr + $y]
        set xy [list 0 $nr]
      }
      NE {
        # we only know what to do when we have an even nr
        set nr [ expr $nr % 2 ? 0 : $nr/2 ]
        set xy [list $nr $nr]
      }
      NW {
        # we only know what to do when we have an even nr
        if { $nr == 2} {
          set x 1
          set y 1
        } elseif { [expr $nr/2 % 2] == 0} {
          set x [expr -$nr /2]
          set y [expr $nr /2]
        } else {
          set x 2
          set y [expr $nr-$x]
        }

        set xy [list -$x $y]
      }
      EL -
      ER {
        set xy [list $nr $y]
      }
      EE {
        set xy [list $nr 0]
      }
      WL -
      WR -
      WW {
        set xy [list -$nr 0]
      }
      SL -
      SR {
        set xy [list $nr $y]
      }
      SS {
        set xy [list 0 -$nr]
      }
      SW {
        # we only know what to do when we have an even nr
        if { $nr == 2} {
          set x 1
          set y 1
        } elseif { [expr $nr/2 % 2] == 0} {
          set x [expr -$nr /2]
          set y [expr $nr /2]
        } else {
          set x 2
          set y [expr $nr-$x]
        }

        set xy [list -$x -$y]
      }
      SE {
        # we only know what to do when we have an even nr
        set nr [ expr $nr % 2 ? 0 : $nr/2 ]
        set xy [list $nr -$nr]
      }
      default {
        puts "xy_displacement_from_wire - unabe to solve direction: ${dir}"
        set xy [list 0 0]
      }
    }
  }
  return $xy
}

########################################################################################
#
# Arguments:
#
# Results:
#
########################################################################################
proc add_xy {xy1 xy2} {
  set x [expr [lindex $xy1 0] + [lindex $xy2 0]]
  set y [expr [lindex $xy1 1] + [lindex $xy2 1]]
  set xy [list $x $y ]
  return $xy
}

proc find_tilewire {tilewire nodes} {
  set i 0
  set index -1

  foreach node $nodes {
    set match [string first $tilewire $node]
    if {$match >= 0} {
     set index $i
     break
    }
    incr i
  }

  return $index
}

########################################################################################
# get the intersection between 2 nets
#
# Arguments:
#   net1: list of absolute net
#   net2: list of absolute net
# Results:
#   the index of intersection in net2
########################################################################################
proc get_net_intersection {net1 net2} {
  set idx -1
  set ignore_list { "\{" "\}" }
  foreach node $net1 {
    if {[lsearch $ignore_list $node] >= 0} {
     continue
    }
    set idx [lsearch $net2 $node]
    if {$idx >= 0} {
    break;
    }
  }
  return $idx
}


########################################################################################
# get the intersection between 2 nets
#
# Arguments:
#   route1: list of absolute nodes from a net
#   route2: list of absolute nodes from a net
# Results:
#   upvar_node is the node found in both nets
#   returns the index of the node in route2
########################################################################################
proc get_node_intersect {route1 route2 upvar_node {first_node 0} {debug 0}} {
  package require struct::set
  upvar $upvar_node node
  set node ""
  set idx -1

  # Hack: remove angle brackets from nodes
  set route1 [lmap i $route1 {lindex [split $i ">"] end}]
  set route2 [lmap i $route2 {lindex [split $i ">"] end}]

  set nodes [::struct::set intersect $route1 $route2]
  ::struct::set exclude nodes "\{"
  ::struct::set exclude nodes "\}"

  # Filter nodes, keep nodes that have tile information
  set nodes [::struct::list filterfor x $nodes {[string first "/" $x ] != -1}]

  if {$first_node} {
    set node [lindex $nodes 0]
    set idx [lsearch $route2 $node]

    if {[llength $nodes] > 1} {
      # Choose the node in route 2 with the lowest index?
      set i -1
      foreach n $nodes {
        set j [lsearch $route2 $n]
        if {($j < $i || $i == -1)} {set i $j;}
      }
      set node [lindex $route2 $i]
      set idx $i
      #logger.info "Took the first node '$node' at index '$i' from route2 out of available nodes: '$nodes'."
    }
  } else {
    set node [lindex $nodes end]
    set idx [lsearch $route2 $node]

    if {[llength $nodes] > 1} {
      foreach n $nodes {
        lappend idx_rt1 [lsearch $route1 $n]
        lappend idx_rt2 [lsearch $route2 $n]
      }

      # When the nodes in rt1 are consecutive, but this does not hold for rt2,
      #  take the first node in front of a open brace
      if {([lconsecutive $idx_rt1] == 1 && [lconsecutive $idx_rt2] == 0)} {

        # Taking the first node in front of an open brace
        set found_one 0
        foreach i $idx_rt2 {
          if {[lindex $route2 $i+1] == "\{" } {
            set node [lindex $route2 $i]
            set idx $i
            set found_one 1
            break;
          }
        }
        if {$found_one != 1} {logger.error "No open brace found."}

      } elseif {!([lconsecutive $idx_rt1] || [lconsecutive $idx_rt2])} {

        # When we have m2m routes, take the node closest to the end
        if {![is_cardinal_wire [lindex $route2 1]]} {
          set idx [lindex $idx_rt2 end]
          set node [lindex $route2 $idx]

          # A more optimal node would be if we choose the last common node in the lowest nested branch.
          # Can we determine this branch depth?
          set node_lowest_branch [find_common_node_lowest_branch $route2 $nodes]
          set idx [lsearch $route2 $node_lowest_branch]
          set node $node_lowest_branch

        } else {
          # When route2 starts with a wire e.g. EE4BEG1, we allow this and return the index of the first node.
          logger.warning "get_node_intersect: Multiple common nodes found: '$nodes'."
        }
      }
    }
  }
  return $idx
}

proc find_common_node_lowest_branch {lnodes lcommon_nodes} {
  set branch_depth 0
  set node_branch_depth -1
  set node_lowest_depth [lindex $lcommon_nodes end]

  foreach n $lnodes {
    # Check if n is a common node
    set i [lsearch $lcommon_nodes $n]

    if {$i != -1} {
      # Update the node depth
      if {$branch_depth <= $node_branch_depth || $node_branch_depth == -1} {
        set node_branch_depth $branch_depth
        set node_lowest_depth $n
      }
    }

    if {$n == "\{"} {incr branch_depth 1}
    if {$n == "\}"} {incr branch_depth -1}
  }

  return $node_lowest_depth
}


########################################################################################
# concatenate 2 nets
#
# Arguments:
#
# Results:
#
########################################################################################
proc concat_nets {net1 net2} {
  foreach node $net2 {
    set net1 [lappend net1 $node]
  }
  return $net1
}

########################################################################################
# compare_node_xy
#
# Arguments:
#
# Results:
#
########################################################################################
proc compare_node_xy {node1 node2} {
  set xy1 [list]
  set xy2 [list]
  get_xy $node1 xy1
  get_xy $node2 xy2

  lassign $xy1 x1 y1
  lassign $xy2 x2 y2

  set z1 [expr $x1+$y1]
  set z2 [expr $x2+$y2]

  if {$z1 < $z2} {
    return -1
  } elseif  {$z1 > $z2} {
    return 1
  }

  return 0
}


########################################################################################
# sort_nodes_xy
#
# Arguments:
#
# Results:
#
########################################################################################
proc sort_nodes_xy {nodes} {
  return [lsort -command compare_node_xy $nodes]
}


########################################################################################
# get cardinal sign
#
# Gets the direction of the cardinal wire. A value of 1 means the direction is going
# away from the origin.
#
# Arguments:
#  cardinal_wire e.g. SS6BEG3
# Results:
#  returns -1 or 1
########################################################################################
proc get_cardinal_sign {cardinal_wire} {
  set dir_neg_sign {NW WW WR WL SS SL SR SE SW}
  set cardinal [list]
  get_cardinal_from_wire $cardinal_wire cardinal
  set dir [lindex $cardinal 0]

  if {[lsearch $dir_neg_sign $dir] >= 0} {
    return -1
  } else {
    return 1
  }
}


########################################################################################
# remove_braces_of_last_branch
#
# Arguments:
#
# Results:
#
########################################################################################
proc remove_braces_of_last_branch {upvar_lnodes} {
  upvar $upvar_lnodes lnodes
  set second_last_node [lindex $lnodes end-1]

  # The second last node is a closing brace.
  if {$second_last_node == "\}"} {
    set i_ob -1
    set cb_count 0
    set start [expr [llength $lnodes] -2]

    # Start at the end of the list to find the matching open brace.
    for {set i $start} {$i > -1} { incr i -1} {
      set brace [lindex $lnodes $i]
      if {$brace == "\}"} {
        incr cb_count 1
      } elseif {$brace == "\{"} {
        incr cb_count -1
        if {$cb_count == 0} {
          # The index for the matching open brace
          set i_ob $i
          break
        }
      }
    }
    if {$i_ob != -1} {
      set lnodes [lreplace $lnodes $i_ob $i_ob]
      set lnodes [lreplace $lnodes end end]
      return 1
    }
  }
  return 0
}


########################################################################################
# find_matching_open_brace
#
# Arguments:
#
# Results:
#
########################################################################################
proc find_matching_open_brace {lnodes index_closing_brace} {
  set cb_count 0
  # Start at the end of the list to find the matching open brace.
  for {set i $index_closing_brace} {$i > -1} { incr i -1} {
    set brace [lindex $lnodes $i]
    if {$brace == "\}"} {
      incr cb_count 1
    } elseif {$brace == "\{"} {
      incr cb_count -1
      if {$cb_count == 0} { return $i}
    }
  }
  return -1
}


########################################################################################
# find_matching_close_brace
#
# Arguments:
#
# Results:
#
########################################################################################
proc find_matching_close_brace {lnodes index_open_brace} {
  set ob_count 0
  # Start at the begin of the list to find the matching close brace.
  for {set i $index_open_brace} {$i < [llength $lnodes]} { incr i 1} {
    set brace [lindex $lnodes $i]
    if {$brace == "\{"} {
      incr ob_count 1
    } elseif {$brace == "\}"} {
      incr ob_count -1
      if {$ob_count == 0} {return $i}
    }
  }
  return -1
}


########################################################################################
# remove_double_close_braces
#
# Arguments:
#
# Results:
#
########################################################################################
proc remove_double_close_braces {upvar_lnodes} {
  upvar $upvar_lnodes lnodes
  set remove_brace 1

  while {$remove_brace} {
    set remove_brace 0
    for {set i 0} {$i < [llength $lnodes]} { incr i 1} {
      set node [lindex $lnodes $i]
      set next_node [lindex $lnodes $i+1]
      if {($node == "\}" && $next_node == "\}")} {
        set ob_i [find_matching_open_brace $lnodes $i]
        if {$ob_i == -1} {error "No matching open brace."}

        # Remove redundant brace
        set lnodes [lreplace $lnodes $i $i]
        set lnodes [lreplace $lnodes $ob_i $ob_i]
        set remove_brace 1
        break
      }
    }
  }
}


########################################################################################
# remove branch from a list of nodes
#
# Arguments:
#
# Results:
#  returns a list of nodes with the branch removed
########################################################################################
proc remove_branch {lnodes node} {
  set nodes $lnodes

  # Find the first closing brace before this node
  set cb_i [lfind_first_element_near_element $lnodes $node \} 1]
  set node_idx [lindex [lsearch $lnodes $node] 0 ]
  if {$cb_i == -1} {error "No close brace found."}
  if {$node_idx == -1} {error "Node not found."}

  set start_i -1
  set end_i $node_idx

  # Find the lowest index before we hit a brace
  for {set i $node_idx} {$i > -1} { incr i -1} {
    set next_node [lindex $lnodes $i-1]

    if {($next_node == "\}" || $next_node == "\{")} {
      set start_i $i

      # Found an open brace: node is part of a single branch
      if {$next_node == "\{"} {
        set start_i [expr {$i-1}]
        set end_i $cb_i
      } else {
        set end_i [expr {$cb_i -1}]
      }
      break
    }
  }

  # Remove the branch
  set nodes [lreplace $nodes $start_i $end_i]

  # Remove any double closing braces
  remove_double_close_braces nodes

  # In case everthing got deleted, return empty node list instead
  if {$nodes == ""} {set nodes [list \{ \}] }

  return $nodes
}



########################################################################################
# remove antenna branch of nets
# Remove the routing for the specified nets that have status ANTENNA.
# We can do this for all nets that only have a single output.
# For nets with status ANTENNA that have also input pins, we can remove a part of the route string.
# Arguments:
#
# Results:
#
########################################################################################
proc remove_antenna_branch_nets {lnets} {
  if {[llength $lnets] > 0} {
    set_property DONT_TOUCH 0 [get_nets $lnets]
    set antenna_nets [get_nets $lnets -filter ROUTE_STATUS==ANTENNAS -quiet]

    logger.debug "Remove antenna branch nets ([llength $antenna_nets])..."

    foreach antenna_net $antenna_nets {
      set direction [get_property DIRECTION [get_pins -of_object [get_nets $antenna_net]]]

      if {[llength $direction] == 1 && [lindex $direction 0] == "OUT"} {
        if {[llength [get_pins -quiet -of_object [get_nets $antenna_net] -filter "DIRECTION==OUT"]] <= 0 } {
          logger.error "Net '$antenna_net' has no OUT pin (no driver)?."
        }

        logger.debug "Clear route of single output net '$antenna_net'..."

        # For nets with single output, we can just clear the entire route string.
        set_property ROUTE {} [get_nets $antenna_net]
      } else {
        # We have to find the antenna node(s) in the net and remove them
        set dnodes [get_abs_route [list [get_nets $antenna_net]]]
        set nodes [dict_first_value dnodes]
        set antenna_nodes [list]

        for {set i 0} {$i < [llength $nodes]} {incr i 1} {
          set node [lindex $nodes $i]
          set next_node [lindex $nodes $i+1]

          if {$next_node == "\}"} {
            if {[is_absolute_wire $node]} {
              set is_pin [get_property IS_PIN [get_nodes $node]]
              if {$is_pin == 0} {
                # Found a node that is only a wire and not a (clb) pin.
                lappend antenna_nodes $node
              }
            }
          }
        }
        foreach antenna_node $antenna_nodes {set nodes [remove_branch $nodes $antenna_node]}

        set route [get_route_string_from_route_list $nodes]

        if {[llength [get_pins -quiet -of_object [get_nets $antenna_net] -filter "DIRECTION==OUT"]] <= 0 } {
          logger.error "Net '$antenna_net' has no OUT pin (no driver)?."
        }
        logger.debug "Clear route of net '$antenna_net'..."
        set_property ROUTE {} [get_nets $antenna_net]
        set_property ROUTE $route [get_nets $antenna_net]
      }
    }
    set_property DONT_TOUCH 1 [get_nets $lnets]
  }
}

########################################################################################
# connect_nets_to_gnd
#
# Arguments:
#
# Result:
#
########################################################################################
proc connect_nets_to_gnd {nets} {
  if {[llength $nets] > 0} {
    set nets [get_nets $nets]
    set_property -dict {{DONT_TOUCH} {0} {ROUTE} {}} [get_nets $nets]
    foreach net $nets {
      set pins [get_pins -quiet -of_object [get_nets $net] -filter "DIRECTION==OUT"]
      if {[llength $pins] != 0} {error "Net '$net' has a driver. Disconnect the driver first."}

      # create a gnd net in the correct hierarchy
      set gnd_net [lindex [split $net "/"] end]; # get last part of the string
      set hier_gnd [join [lreplace [split $net "/"] end end] "/"]; # get the first parts of the string
      if {[string length $hier_gnd] > 0} {set hier_gnd "${hier_gnd}/"}
      set gnd "${hier_gnd}gnd_${gnd_net}"

      create_cell -reference GND $gnd
      connect_net -hier -net $net -objects "${gnd}/G"
    }
    set_property DONT_TOUCH 1 [get_nets $nets]
  }
}


########################################################################################
# get first node in region
#
# Arguments:
#  lroute:  list of of nodes that form the route string
#  lregion:
# Result:
#
########################################################################################
proc get_first_node_in_region {lroute topleft_x topleft_y bottomright_x bottomright_y upvar_node} {
  upvar $upvar_node first_node
  set i 0
  foreach node $lroute {
    if {[get_xy $node xy]} {
      set x [lindex $xy 0]
      set y [lindex $xy 1]

      if {($x >= $topleft_x && $x <= $bottomright_x && $y >= $bottomright_y && $y <= $topleft_y)} {
        set first_node $node
        return $i
        break
      }
    }
    incr i 1
  }
  return -1
}


########################################################################################
# clear route string
#
# Arguments:
#  net: the net to clear the route string from
# Result:
#  sets the route property to an empty string
########################################################################################
proc clear_route {net} {set_property ROUTE {} [get_nets $net]}


########################################################################################
# get error nets
#
# Arguments:
#  none
# Result:
#  returns all nets that have an error status
########################################################################################
proc get_error_nets {} {return [get_nets -filter "ROUTE_STATUS!=ROUTED && ROUTE_STATUS!=INTRASITE"]}


########################################################################################
# get lut pin assignments
#
# Arguments:
#  none
# Result:
#  returns a dict where the key is a LUT input pin and the value Slice/LUT/Input
########################################################################################
proc get_lut_pin_assignments {} {
  set lut_pin_assignments [dict create]
  set pins [get_pins -of_object [get_cells -hierarchical -filter { PRIMITIVE_TYPE =~ LUT.*.* }]]
  foreach pin $pins {
    set bel_pin  [get_bel_pins -of_objects $pin]
    dict set lut_pin_assignments $pin $bel_pin
  }
  return $lut_pin_assignments
}


########################################################################################
# set lut pin assignments
# in conjunction with get_lut_pin_assignments
# Arguments:
#  lut_pin_assignments:  a dict where the key is a LUT input pin and the value Slice/LUT/Input
# Result:
#  none
########################################################################################
proc set_lut_pin_assignments {lut_pin_assignments inst_partialarea} {
  # Create a dict with all the cell lock pins
  set cells_lockpins [dict create]
  dict for {cell_pin site_lutpin} $lut_pin_assignments {
    set cell [string range $cell_pin 0 [expr {[string last "/" $cell_pin] -1}]]
    set pin [lindex [split $cell_pin "/"] end]
    set lutpin [lindex [split $site_lutpin "/"] end]

    if {[regexp {[I][0-6]} $pin]} {
      set lockpin "$pin:$lutpin"
      dict_append_values cells_lockpins $cell $lockpin
    }
  }

  # Try to restore every LOCK_PIN property for each LUT cell in the static design
  logger.debug "Restore all lockpins LUTs ([dict size $cells_lockpins])..."
  dict for {cell lock_pins} $cells_lockpins {
    # Exclude cells in the partial area, these where used for anchor logic
    if {[string first $inst_partialarea $cell] == -1} {
      if {[get_cells $cell] != ""} {
        logger.info "Restore LOCK_PINS of cell: $cell"
        if { [catch {set_property LOCK_PINS $lock_pins [get_cells $cell]} res opt] } {
          logger.warning "Could not restore lockpin of cell $cell: $res"
        }
      }
    }
  }
}

proc get_range_cell {cell} {
  set xy_range [list {0 0 0 0}]; #xy_min xy_max
  set tiles [get_tiles -of_objects [get_sites -of_objects [get_pblocks -of_objects [get_cells -quiet $cell/*] -quiet] -quiet] -quiet]
  if {[llength $tiles] > 0} {
    set xmin 0
    set xmax 0
    set ymin 0
    set ymax 0
    set init 0

    # Find the min and max x-y coordinates from the list of tiles
    foreach tile $tiles {

      if {[get_xy $tile xy]==1} {
        set x [lindex $xy 0]
        set y [lindex $xy 1]
        if {$init == 0} {
          set xmin $x
          set ymin $y
          set init 1
        }
        if {$x < $xmin} {set xmin $x}
        if {$y < $ymin} {set ymin $y}
        if {$x > $xmax} {set xmax $x}
        if {$y > $ymax} {set ymax $y}
      }
    }

     set xy_range [list $xmin $ymin $xmax $ymax];
  }

  return $xy_range
}

